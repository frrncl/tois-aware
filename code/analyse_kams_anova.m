%% analyse_kams_anova
% 
% Computes a four factors, mixed effects, repeated measures GLMM
% considering kuplse as random factor (repeated measures/within-subject) 
% and EXPERIMENT.taxonomy.approaches, measures, and systems as fixed factors.
%
%% Synopsis
%
%   [] = analyse_kams_anova(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = analyse_kams_anova(trackID)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;
                
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('analyse_kams_anova', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('analyse_kams_anova', trackID));
    
    % the ANOVA factors label
    factors = 'kams';
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing %s ANOVA analyses on collection %s (%s) ########\n\n', upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    fprintf('+ Loading analyses\n');
    
    % total number of elements in the list
    N = (EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * ...
        EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number + ...
        EXPERIMENT.taxonomy.baseline.number) * (originalTrackNumber * EXPERIMENT.kuples.number * EXPERIMENT.measure.number) ;
    
    % preallocate vectors for each measure
    data.apc = NaN(1, N);  % the apc data
    data.rmse = NaN(1, N);  % the apc data
    subject = cell(1, N);  % grouping variable for the subjects (kuple)
    factorA = cell(1, N);  % grouping variable for factorA (EXPERIMENT.taxonomy.approaches)
    factorB = cell(1, N);  % grouping variable for factorB (measure)
    factorC = cell(1, N);  % grouping variable for factorC (systems)
    
    % the current element in the list
    currentElement = 1;

    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            measureStart = currentElement;
            
            for k = 1:EXPERIMENT.kuples.number
                
                fprintf('      # k-uple: %s \n', EXPERIMENT.pattern.identifier.kuple(k));
                
                for g = 1:EXPERIMENT.taxonomy.granularity.number
                    for a = 1:EXPERIMENT.taxonomy.aggregation.number
                        for gp = 1:EXPERIMENT.taxonomy.gap.number
                            for w = 1:EXPERIMENT.taxonomy.weight.number
                                
                                e = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', ...
                                    EXPERIMENT.taxonomy.granularity.list{g}, ...
                                    EXPERIMENT.taxonomy.aggregation.list{a}, ...
                                    EXPERIMENT.taxonomy.gap.list{gp}, ...
                                    EXPERIMENT.taxonomy.weight.list{w});
                                
                                %fprintf('        experiment: %s \n', e);
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID), ...
                                    {apcID, 'apcTmp';
                                    rmseID, 'rmseTmp'});
                                
                                data.apc(currentElement) = apcTmp.mean;
                                data.rmse(currentElement) = rmseTmp.mean;
                                subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                                factorA{currentElement} = EXPERIMENT.tag.(e).matlab.label;
                                factorB{currentElement} = EXPERIMENT.measure.name{m};
                                factorC{currentElement} = originalTrackShortID{r};
                                
                                currentElement = currentElement + 1;
                                
                                clear apcTmp rmseTmp
                                
                            end; % weight
                        end; % gap
                    end; % aggregation
                end; % granularity
                
                % add the baselines
                for e = 1:EXPERIMENT.taxonomy.baseline.number
                    
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(k), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    
                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).relativePath,  EXPERIMENT.taxonomy.baseline.list{e}, measureID), ...
                        {apcID, 'apcTmp';
                        rmseID, 'rmseTmp'});
                    
                    data.apc(currentElement) = apcTmp.mean;
                    data.rmse(currentElement) = rmseTmp.mean;
                    subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                    factorA{currentElement} =  EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).matlab.label;
                    factorB{currentElement} = EXPERIMENT.measure.name{m};
                    factorC{currentElement} = originalTrackShortID{r};
                    
                    
                    currentElement = currentElement + 1;
                    
                    clear apcTmp rmseTmp;
                    
                end % additional tags
                
            end % kuples
            
            % normalize RMSE data by the maximum for that measure
            data.rmse(measureStart:currentElement-1) = data.rmse(measureStart:currentElement-1) ./ max(data.rmse(measureStart:currentElement-1));
            
        end % measures
    end; % for each runset
    
        
    fprintf('+ Computing the analyses\n');    

    % the model = Kuple + Approach + Measure + Systems + 
    %             Approach*Measure + Approach*Systems + Measure*Systems
    % 
    m = [1 0 0 0; ...
         0 1 0 0; ...
         0 0 1 0; ...
         0 0 0 1; ...
         0 1 1 0; ...
         0 1 0 1; ...
         0 0 1 1];
    
    % compute a 3-way ANOVA for AP correlation
    [~, apcTbl, apcStats] = anovan(data.apc, {subject, factorA, factorB, factorC}, 'Model', m, ...
        'VarNames', {'K-uple Size', 'Approach', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    
    df_approach = apcTbl{3,3};
    ss_approach = apcTbl{3,2};
    F_approach = apcTbl{3,6};
    
    df_measure = apcTbl{4,3};
    ss_measure = apcTbl{4,2};
    F_measure = apcTbl{4,6};
    
    df_systems = apcTbl{5,3};
    ss_systems = apcTbl{5,2};
    F_systems = apcTbl{5,6};
    
    df_approach_measure = apcTbl{6,3};
    ss_approach_measure = apcTbl{6,2};
    F_approach_measure = apcTbl{6,6};
    
    df_approach_systems = apcTbl{7,3};
    ss_approach_systems = apcTbl{7,2};
    F_approach_systems = apcTbl{7,6};
    
    df_measure_systems = apcTbl{8,3};
    ss_measure_systems = apcTbl{8,2};
    F_measure_systems = apcTbl{8,6};
    
    ss_error = apcTbl{9, 2};
    df_error = apcTbl{9, 3};
    
    ss_total = apcTbl{10, 2};
    
    
    soa.omega2.apc.approach = df_approach * (F_approach - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.measure = df_measure * (F_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.systems = df_systems * (F_systems - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.approach_systems = df_approach_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.apc.measure_systems = df_measure_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    
    
    soa.omega2p.apc.approach = df_approach * (F_approach - 1) / (df_approach * (F_approach - 1) + N);
    soa.omega2p.apc.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.apc.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.apc.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach_measure * (F_approach_measure - 1) + N);
    soa.omega2p.apc.approach_systems = df_approach_systems * (F_approach_systems - 1) / (df_approach_systems * (F_approach_systems - 1) + N);
    soa.omega2p.apc.measure_systems = df_measure_systems * (F_measure_systems - 1) / (df_measure_systems * (F_measure_systems - 1) + N);
    
    soa.eta2.apc.approach = ss_approach / ss_total;
    soa.eta2.apc.measure = ss_measure / ss_total;
    soa.eta2.apc.systems = ss_systems / ss_total;
    soa.eta2.apc.approach_measure = ss_approach_measure / ss_total;
    soa.eta2.apc.approach_systems = ss_approach_systems / ss_total;
    soa.eta2.apc.measure_systems = ss_measure_systems / ss_total;
    
    soa.eta2p.apc.approach = ss_approach / (ss_approach + ss_error);
    soa.eta2p.apc.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.apc.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2p.apc.approach_measure = ss_approach_measure / (ss_approach_measure + ss_error);
    soa.eta2p.apc.approach_systems = ss_approach_systems / (ss_approach_systems + ss_error);
    soa.eta2p.apc.measure_systems = ss_measure_systems / (ss_measure_systems + ss_error);
    

    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.apc(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.apc(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.apc(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.apc(:), factorC(:), {'mean', 'meanci'});
    
    % interaction between kuple size (x-axis) and approach (y-axis)
    % each row is an approach, columns are kuple size
    ie.factorSA.mean = grpstats(data.apc(:), {subject(:),factorA(:)}, {'mean'});
    ie.factorSA.mean = reshape(ie.factorSA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.kuples.number).';
    
    
    % interaction between measure (x-axis) and approach (y-axis)
    % each row is a approach, columns are measures
    ie.factorBA.mean = grpstats(data.apc(:), {factorB(:), factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.measure.number).';
        
    % interaction between systems (x-axis) and approach (y-axis)
    % each row is an approch, columns are systems
    ie.factorCA.mean = grpstats(data.apc(:), {factorC(:), factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.taxonomy.approaches, originalTrackNumber).';
    
    
    yMin = Inf;
    yMax = -Inf;
    
    currentFigure = figure('Visible', 'off');
 
    subplot(1, 4, 1);
    
        xTick = 1:EXPERIMENT.taxonomy.approaches;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax4 = gca;
        ax4.TickLabelInterpreter = 'none';
        ax4.FontSize = 24;
       
        ax4.XTick = xTick;
        ax4.XTickLabel = Alabels;
        ax4.XTickLabelRotation = 90;
        
        ax4.XLabel.Interpreter = 'tex';
        ax4.XLabel.String = 'Approach';
        
        ax4.XGrid = 'on';
        
        ax4.YLabel.Interpreter = 'tex';
        ax4.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax4.YLim(1));
        yMax = max(yMax, ax4.YLim(2));    
        
    
    subplot(1, 4, 2);
    
        xTick = 1:EXPERIMENT.kuples.number;
        plotData.mean = me.subject.mean.';
        plotData.ciLow = me.subject.ci(:, 1).';
        plotData.ciHigh = me.subject.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = xTick;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        
        ax1.XGrid = 'on';
        
        %ax1.YLabel.Interpreter = 'tex';
        %ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));
    
   subplot(1, 4, 3);
    
        xTick = 1:EXPERIMENT.measure.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = xTick;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
                
        
        %ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));        
        
    subplot(1, 4, 4);
    
        xTick = 1:originalTrackNumber;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = xTick;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Systems';
        
        
        %ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            

    
        ax1.YLim = [yMin yMax];
       
        ax2.YLim = [yMin yMax];
    
        ax3.YLim = [yMin yMax];
        
        ax4.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [162 40];
        currentFigure.PaperPosition = [1 1 160 38];   
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'mainEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;
        
        
    currentFigure = figure('Visible', 'off');
    
        % compare all the possible pairs
        [c, d, ~, nms] = multcompare(apcStats,'ctype', 'hsd', 'Dimension', [2], 'alpha', 0.05, 'display','on');
    
        
        ax = gca;
        ax.FontSize = 24;
        
        ax.Title.String = '';
        ax.Title.Interpreter = 'tex';
    
        ax.TickLabelInterpreter = 'none';
    
        ax.XLabel.String = sprintf('AP Correlation Marginal Mean');
        ax.XLabel.Interpreter = 'tex';
        
        ax.YLabel.String = 'Approach';
        ax.YLabel.Interpreter = 'tex';
    
        ax.YTickLabel = Alabels(end:-1:1);
        
        ax.YGrid = 'on';
        
        highlightTopGroup(currentFigure, d, true);
    
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 42];
        currentFigure.PaperPosition = [1 1 40 40];  
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'approachMultcompare', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        
    yMin = Inf;
    yMax = -Inf;                 
        
    currentFigure = figure('Visible', 'off');
        
   subplot(1, 3, 1);
    
        plotData = ie.factorSA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = 1:EXPERIMENT.kuples.number;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        ax1.XGrid = 'on';
        
        title('K-uple Size*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));  
    
    subplot(1, 3, 2);
    
        plotData = ie.factorBA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = 1:EXPERIMENT.measure.number;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
        
        ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        title('Measure*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));    
        
    subplot(1, 3, 3);
    
        plotData = ie.factorCA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = 1:originalTrackNumber;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Systems';
        
        ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        title('Systems*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            
    
        
        ax1.YLim = [yMin yMax];
        ax2.YLim = [yMin yMax];
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [162 40];
        currentFigure.PaperPosition = [1 1 160 38];   
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'interactionEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;
        
   % compute a 3-way ANOVA for RMSE
    [~, rmseTbl, rmseStats] = anovan(data.rmse, {subject, factorA, factorB, factorC}, 'Model', m, ...
        'VarNames', {'Kuple', 'Approach', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    
    df_approach = rmseTbl{3,3};
    ss_approach = rmseTbl{3,2};
    F_approach = rmseTbl{3,6};
    
    df_measure = rmseTbl{4,3};
    ss_measure = rmseTbl{4,2};
    F_measure = rmseTbl{4,6};
    
    df_systems = rmseTbl{5,3};
    ss_systems = rmseTbl{5,2};
    F_systems = rmseTbl{5,6};
    
    df_approach_measure = rmseTbl{6,3};
    ss_approach_measure = rmseTbl{6,2};
    F_approach_measure = rmseTbl{6,6};
    
    df_approach_systems = rmseTbl{7,3};
    ss_approach_systems = rmseTbl{7,2};
    F_approach_systems = rmseTbl{7,6};
    
    df_measure_systems = rmseTbl{8,3};
    ss_measure_systems = rmseTbl{8,2};
    F_measure_systems = rmseTbl{8,6};
    
    ss_error = rmseTbl{9, 2};
    df_error = rmseTbl{9, 3};
    
    ss_total = rmseTbl{10, 2};
    
    
    soa.omega2.rmse.approach = df_approach * (F_approach - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.measure = df_measure * (F_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.systems = df_systems * (F_systems - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.approach_systems = df_approach_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    soa.omega2.rmse.measure_systems = df_measure_systems * (F_approach_measure - 1) / (df_approach * F_approach + df_measure * F_measure + df_systems * F_systems + df_approach_measure * F_approach_measure + df_approach_systems * F_approach_systems + df_measure_systems * F_measure_systems + df_error + 1);
    
    
    soa.omega2p.rmse.approach = df_approach * (F_approach - 1) / (df_approach * (F_approach - 1) + N);
    soa.omega2p.rmse.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.rmse.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.rmse.approach_measure = df_approach_measure * (F_approach_measure - 1) / (df_approach_measure * (F_approach_measure - 1) + N);
    soa.omega2p.rmse.approach_systems = df_approach_systems * (F_approach_systems - 1) / (df_approach_systems * (F_approach_systems - 1) + N);
    soa.omega2p.rmse.measure_systems = df_measure_systems * (F_measure_systems - 1) / (df_measure_systems * (F_measure_systems - 1) + N);
    
    soa.eta2.rmse.approach = ss_approach / ss_total;
    soa.eta2.rmse.measure = ss_measure / ss_total;
    soa.eta2.rmse.systems = ss_systems / ss_total;
    soa.eta2.rmse.approach_measure = ss_approach_measure / ss_total;
    soa.eta2.rmse.approach_systems = ss_approach_systems / ss_total;
    soa.eta2.rmse.measure_systems = ss_measure_systems / ss_total;
    
    soa.eta2p.rmse.approach = ss_approach / (ss_approach + ss_error);
    soa.eta2p.rmse.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.rmse.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2p.rmse.approach_measure = ss_approach_measure / (ss_approach_measure + ss_error);
    soa.eta2p.rmse.approach_systems = ss_approach_systems / (ss_approach_systems + ss_error);
    soa.eta2p.rmse.measure_systems = ss_measure_systems / (ss_measure_systems + ss_error);    

  
    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.rmse(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.rmse(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.rmse(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.rmse(:), factorC(:), {'mean', 'meanci'});
    
    % interaction between kuple size (x-axis) and approach (y-axis)
    % each row is an approach, columns are kuple size
    ie.factorSA.mean = grpstats(data.rmse(:), {subject(:),factorA(:)}, {'mean'});
    ie.factorSA.mean = reshape(ie.factorSA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.kuples.number).';
    
    
    % interaction between measure (x-axis) and approach (y-axis)
    % each row is a approach, columns are measures
    ie.factorBA.mean = grpstats(data.rmse(:), {factorB(:), factorA(:)}, {'mean'});
    ie.factorBA.mean = reshape(ie.factorBA.mean, EXPERIMENT.taxonomy.approaches, EXPERIMENT.measure.number).';
        
    % interaction between systems (x-axis) and approach (y-axis)
    % each row is an approch, columns are systems
    ie.factorCA.mean = grpstats(data.rmse(:), {factorC(:), factorA(:)}, {'mean'});
    ie.factorCA.mean = reshape(ie.factorCA.mean, EXPERIMENT.taxonomy.approaches, originalTrackNumber).';
    
    
    yMin = Inf;
    yMax = -Inf;
    
    currentFigure = figure('Visible', 'off');
 
    subplot(1, 4, 1);
    
        xTick = 1:EXPERIMENT.taxonomy.approaches;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax4 = gca;
        ax4.TickLabelInterpreter = 'none';
        ax4.FontSize = 24;
       
        ax4.XTick = xTick;
        ax4.XTickLabel = Alabels;
        ax4.XTickLabelRotation = 90;
        
        ax4.XLabel.Interpreter = 'tex';
        ax4.XLabel.String = 'Approach';
        
        ax4.XGrid = 'on';
        
        ax4.YLabel.Interpreter = 'tex';
        ax4.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax4.YLim(1));
        yMax = max(yMax, ax4.YLim(2));    
        
    
    subplot(1, 4, 2);
    
        xTick = 1:EXPERIMENT.kuples.number;
        plotData.mean = me.subject.mean.';
        plotData.ciLow = me.subject.ci(:, 1).';
        plotData.ciHigh = me.subject.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = xTick;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        
        ax1.XGrid = 'on';
        
        %ax1.YLabel.Interpreter = 'tex';
        %ax1.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));
    
   subplot(1, 4, 3);
    
        xTick = 1:EXPERIMENT.measure.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = xTick;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
                
        
        %ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));        
        
    subplot(1, 4, 4);
    
        xTick = 1:originalTrackNumber;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = xTick;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Systems';
        
        
        %ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            

    
        ax1.YLim = [yMin yMax];
       
        ax2.YLim = [yMin yMax];
    
        ax3.YLim = [yMin yMax];
        
        ax4.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [162 40];
        currentFigure.PaperPosition = [1 1 160 38];   
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'mainEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;
        
        
    currentFigure = figure('Visible', 'off');
    
        % compare all the possible pairs
        [c, d, ~, nms] = multcompare(rmseStats,'ctype', 'hsd', 'Dimension', [2], 'alpha', 0.05, 'display','on');
    
        
        ax = gca;
        ax.FontSize = 24;
        
        ax.Title.String = '';
        ax.Title.Interpreter = 'tex';
    
        ax.TickLabelInterpreter = 'none';
    
        ax.XLabel.String = sprintf('Normalized RMSE Marginal Mean');
        ax.XLabel.Interpreter = 'tex';
        
        ax.YLabel.String = 'Approach';
        ax.YLabel.Interpreter = 'tex';
    
        ax.YTickLabel = Alabels(end:-1:1);
        
        ax.YGrid = 'on';
        
        highlightTopGroup(currentFigure, d, false);
    
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 42];
        currentFigure.PaperPosition = [1 1 40 40];  
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'approachMultcompare', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        
    yMin = Inf;
    yMax = -Inf;                 
        
    currentFigure = figure('Visible', 'off');
        
   subplot(1, 3, 1);
    
        plotData = ie.factorSA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:EXPERIMENT.kuples.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = 1:EXPERIMENT.kuples.number;
        ax1.XTickLabel = Slabels;
        ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'K-uple Size';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        ax1.XGrid = 'on';
        
        title('K-uple Size*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));  
    
    subplot(1, 3, 2);
    
        plotData = ie.factorBA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:EXPERIMENT.measure.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = 1:EXPERIMENT.measure.number;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Measure';
        
        ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        title('Measure*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));    
        
    subplot(1, 3, 3);
    
        plotData = ie.factorCA.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.approaches 
            if (l < EXPERIMENT.taxonomy.approaches - EXPERIMENT.taxonomy.baseline.number + 1)
                plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 0.9);                
            else
                plot(1:originalTrackNumber, plotData(l, :), 'Color', EXPERIMENT.taxonomy.colors(l, :), 'LineWidth', 2.0);                
            end;
        end;
            
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = 1:originalTrackNumber;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Systems';
        
        ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        title('Systems*Approach Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Alabels, 'Location', 'BestOutside', 'Interpreter', 'none')
        
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            
    
        
        ax1.YLim = [yMin yMax];
        ax2.YLim = [yMin yMax];
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [162 40];
        currentFigure.PaperPosition = [1 1 160 38];   
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'interactionEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)    
    
        clear plotData;
    

        
    anovaID = EXPERIMENT.pattern.identifier.anova.id(factors, shortTrackID);
    
    sersave(EXPERIMENT.pattern.file.anova(trackID, factors, anovaID), 'apcTbl', 'apcStats', 'rmseTbl', 'rmseStats', 'soa');
                
    fprintf('\n\n######## Total elapsed time for computing %s ANOVA analyses on collection %s (%s): %s ########\n\n', ...
            upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end



function [] =  highlightTopGroup(h, m, usemax)

    common_parameters;
    
    % the index of the top element in the top group
    if usemax
        [~, topElement] = max(m(:, 1));
    else
        [~, topElement] = min(m(:, 1));
    end;
    
    % Get bounds for the top group
    topGroupHandle = findobj(h, 'UserData', -topElement);
    if (isempty(topGroupHandle))
        return; 
    end % unexpected

    topGroupX = get(topGroupHandle, 'XData');
    topGroupLower = min(topGroupX);
    topGroupUpper = max(topGroupX);
    
    % Change the comparison lines to use these values
    comparisonLines = findobj(h, 'UserData', 'Comparison lines');
    if (~isempty(comparisonLines))
       comparisonLines.LineWidth = 2;
       comparisonLines.XData(1:2) = topGroupLower;
       comparisonLines.XData(4:5) = topGroupUpper;
    end

    % arrange line styles and markers
    for e=1:(EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * ...
        EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number + ...
        EXPERIMENT.taxonomy.baseline.number)

        % look for the line and marker of the current element
        lineHandle = findobj(h, 'UserData', -e, 'Type','Line', '-and', 'LineStyle', '-', '-and', 'Marker', 'none');
        markerHandle = findobj(h, 'UserData', e, 'Type','Line', '-and', 'LineStyle', 'none', '-and', 'Marker', 'o');

        if (isempty(lineHandle))
            continue;
        end  % unexpected

        currentElementX = get(lineHandle, 'XData');
        currentElementLower = min(currentElementX);
        currentElementUpper = max(currentElementX);
        
        % To be in the top group the upper bound of the current element
        % (CEL) must be above the lower bound of the top group (TGL) and
        % the lower bound of the current element (CEL) must be below the
        % upper bound of the top groud (TGL)
        %
        %  CEL    TGL   CEU  TGU
        %   |      |_____|____|
        %   |____________|
        %
        %  TGL   CEL  TGU     CEU
        %   |_____|____|       |
        %         |____________|
        %
        if ( currentElementUpper > topGroupLower && currentElementLower < topGroupUpper)
            lineHandle.Color = EXPERIMENT.taxonomy.colors(e, :);
            lineHandle.LineWidth = 2.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = EXPERIMENT.taxonomy.colors(e, :);
            markerHandle.MarkerEdgeColor = EXPERIMENT.taxonomy.colors(e, :);
            markerHandle.MarkerSize = 10;
        else
            lineHandle.Color = 'k';
            lineHandle.LineWidth = 1.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = 'k';
            markerHandle.MarkerEdgeColor = 'k';
            markerHandle.MarkerSize = 6;
        end;
        
        % highlight state-of-art baselines
        if any(strcmp(h.Children.YTickLabel(end-markerHandle.UserData+1), strrep(EXPERIMENT.taxonomy.baseline.list, 'aw_', '')));
            lineHandle.LineWidth = 2.0;
            lineHandle.Color = EXPERIMENT.taxonomy.colors(e, :);
            markerHandle.MarkerSize = 8;
            markerHandle.MarkerFaceColor = EXPERIMENT.taxonomy.colors(e, :);
            markerHandle.MarkerEdgeColor = EXPERIMENT.taxonomy.colors(e, :);
        end;
        
    end; % for tags

 
end


