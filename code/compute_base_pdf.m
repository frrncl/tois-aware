%% compute_base_measures_pdf
% 
% Computes the KDE of PDF for measures computed on the base pools and saves 
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_base_pdf(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|granularity|* - the granularity of the coefficients, either sgl or tpc.
% * *|aggregation|* - the aggregation of the coefficients, either msr or gp.
%
%
% *Returns*
%
% Nothing
%


%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_base_pdf(trackID, granularity, aggregation)

     % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that granularity is a non-empty string
    validateattributes(granularity,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'granularity');
    
    if iscell(granularity)
        % check that granularity is a cell array of strings with one element
        assert(iscellstr(granularity) && numel(granularity) == 1, ...
            'MATTERS:IllegalArgument', 'Expected granularity to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    granularity = char(strtrim(granularity));
    granularity = granularity(:).';
    
    % check that aggregation is a non-empty string
    validateattributes(aggregation,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'aggregation');
    
    if iscell(aggregation)
        % check that aggregation is a cell array of strings with one element
        assert(iscellstr(aggregation) && numel(aggregation) == 1, ...
            'MATTERS:IllegalArgument', 'Expected aggregation to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    aggregation = char(strtrim(aggregation));
    aggregation = aggregation(:).';
    
    % setup common parameters
    common_parameters;

    delete(EXPERIMENT.pattern.logFile.computeMeasuresPdf(EXPERIMENT.tag.base.id, trackID));
    diary(EXPERIMENT.pattern.logFile.computeMeasuresPdf(EXPERIMENT.tag.base.id, trackID));

    % start of overall computations
    startComputation = tic;


    fprintf('\n\n######## Computing %s %s %s measures PDF on collection %s (%s) ########\n\n', granularity, aggregation, EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - granularity %s\n', granularity);
    fprintf('  - aggregation %s\n\n', aggregation);


    % Load the number of pools and topics
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'poolLabels');
    
    % Adding the primary pool labels to run everything in one cycle
    poolLabels = [{EXPERIMENT.label.goldPool} poolLabels];

    P = length(poolLabels);

    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        
        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        
        for m = 1:EXPERIMENT.measure.number
            
            start = tic;
            
            fprintf('  - %s PDF for %s\n', EXPERIMENT.tag.base.id, EXPERIMENT.measure.id{m});
            
            for p = 1:P
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID), measureID);
                
                eval(sprintf('assessor = %1$s{:, :};', measureID));                
                
                clear(measureID);
                
                pdfID = EXPERIMENT.pattern.identifier.pm('pdf', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, poolLabels{p}, ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                
                 % single gap for all the topics
                if strcmpi(granularity, 'sgl')
                    
                    
                    evalf(EXPERIMENT.command.rndPools.gap.(granularity).kde, ...
                        {'assessor'}, ...
                        {pdfID});
                    
                else
                    
                    T = size(assessor, 1);
                    
                    % one gap for each topic
                    eval(sprintf('%1$s = NaN(T, length(EXPERIMENT.analysis.kdeBins));', pdfID));
                    
                    for t = 1:T
                        
                        assessorTopic = assessor(t, :);
                        
                        evalf(EXPERIMENT.command.rndPools.gap.(granularity).kde, ...
                            {'assessorTopic'}, ...
                            {'tmp'});
                        
                        eval(sprintf('%1$s(t, :) = tmp;', pdfID));
                        
                    end;
                    
                    clear assessorTopic tmp;
                    
                end;
                
                sersave(EXPERIMENT.pattern.file.pre(granularity, aggregation, 'pdf', trackID, EXPERIMENT.tag.base.id, pdfID), pdfID(:));
                
                clear(pdfID);
                
            end;
            
            fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            
        end; % for measure
    end; % for runset
    

    fprintf('\n\n######## Total elapsed time for computing %s %s %s measures PDF on collection %s (%s): %s ########\n\n', ...
            granularity, aggregation, EXPERIMENT.tag.base.id, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;

end
