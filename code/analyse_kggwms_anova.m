%% analyse_kggwms_anova
% 
% Computes a size factors, mixed effects, repeated measures GLMM
% considering kuplse as random factor (repeated measures/within-subject) 
% and granularity, gap, weight, measures, and systems as fixed factors.
%
%% Synopsis
%
%   [] = analyse_kggwms_anova(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = analyse_kggwms_anova(trackID)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;
                
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('analyse_kggwms_anova', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('analyse_kggwms_anova', trackID));
    
    % the ANOVA factors label
    factors = 'kggwms';
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing %s ANOVA analyses on collection %s (%s) ########\n\n', upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    fprintf('+ Loading analyses\n');
    
    % total number of elements in the list
    N = (EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * ...
        EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number ) * (originalTrackNumber * EXPERIMENT.kuples.number * EXPERIMENT.measure.number) ;
    
    % preallocate vectors for each measure
    data.apc = NaN(1, N);  % the apc data
    data.rmse = NaN(1, N);  % the apc data
    subject = cell(1, N);  % grouping variable for the subjects (kuple)
    factorA = cell(1, N);  % grouping variable for factorA (granularity)
    factorB = cell(1, N);  % grouping variable for factorB (gap)
    factorC = cell(1, N);  % grouping variable for factorC (weight)
    factorD = cell(1, N);  % grouping variable for factorD (measure)
    factorE = cell(1, N);  % grouping variable for factorE (systems)
    
    % the current element in the list
    currentElement = 1;

    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            measureStart = currentElement;
            
            for k = 1:EXPERIMENT.kuples.number
                
                fprintf('      # k-uple: %s \n', EXPERIMENT.pattern.identifier.kuple(k));
                
                for g = 1:EXPERIMENT.taxonomy.granularity.number
                    for a = 1:EXPERIMENT.taxonomy.aggregation.number
                        for gp = 1:EXPERIMENT.taxonomy.gap.number
                            for w = 1:EXPERIMENT.taxonomy.weight.number
                                
                                e = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', ...
                                    EXPERIMENT.taxonomy.granularity.list{g}, ...
                                    EXPERIMENT.taxonomy.aggregation.list{a}, ...
                                    EXPERIMENT.taxonomy.gap.list{gp}, ...
                                    EXPERIMENT.taxonomy.weight.list{w});
                                
                                %fprintf('        experiment: %s \n', e);
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID), ...
                                    {apcID, 'apcTmp';
                                    rmseID, 'rmseTmp'});
                                
                                data.apc(currentElement) = apcTmp.mean;
                                data.rmse(currentElement) = rmseTmp.mean;
                                subject{currentElement} = EXPERIMENT.pattern.identifier.kuple(k);
                                factorA{currentElement} = EXPERIMENT.taxonomy.granularity.list{g};
                                factorB{currentElement} = EXPERIMENT.taxonomy.gap.list{gp};
                                factorC{currentElement} = EXPERIMENT.taxonomy.weight.list{w};
                                factorD{currentElement} = EXPERIMENT.measure.name{m};
                                factorE{currentElement} = originalTrackShortID{r};
                                
                                
                                currentElement = currentElement + 1;
                                
                                clear apcTmp rmseTmp
                                
                            end; % weight
                        end; % gap
                    end; % aggregation
                end; % granularity                                              
            end % kuples
            
            % normalize RMSE data by the maximum for that measure
            data.rmse(measureStart:currentElement-1) = data.rmse(measureStart:currentElement-1) ./ max(data.rmse(measureStart:currentElement-1));
            
        end % measures
    end; % for each runset
    
        
    fprintf('+ Computing the analyses\n');    
    
    % the model = Kuple + Granularity + Gap + Weight + Measure + Systems + 
    %             Granularity*Gap + Granularity*Weight + Gap*Weight
    % 
    m = [1 0 0 0 0 0; ...
         0 1 0 0 0 0; ...
         0 0 1 0 0 0; ...
         0 0 0 1 0 0; ...
         0 0 0 0 1 0; ...
         0 0 0 0 0 1; ...
         0 1 1 0 0 0; ...
         0 1 0 1 0 0; ...
         0 0 1 1 0 0];
  
    
    % compute a 6-way ANOVA for AP correlation
    [~, apcTbl, apcStats] = anovan(data.apc, {subject, factorA, factorB, factorC, factorD, factorE}, 'Model', m, ...
        'VarNames', {'K-uple Size', 'Granularity', 'Gap', 'Weight', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    
    df_granularity = apcTbl{3,3};
    ss_granularity = apcTbl{3,2};
    F_granularity = apcTbl{3,6};
    
    df_gap = apcTbl{4,3};
    ss_gap = apcTbl{4,2};
    F_gap = apcTbl{4,6};
    
    df_weight = apcTbl{5,3};
    ss_weight = apcTbl{5,2};
    F_weight = apcTbl{5,6};
    
    df_measure = apcTbl{6,3};
    ss_measure = apcTbl{6,2};
    F_measure = apcTbl{6,6};
    
    df_systems = apcTbl{7,3};
    ss_systems = apcTbl{7,2};
    F_systems = apcTbl{7,6};
    
    df_granularity_gap = apcTbl{8,3};
    ss_granularity_gap = apcTbl{8,2};
    F_granularity_gap = apcTbl{8,6};
    
    df_granularity_weight = apcTbl{9,3};
    ss_granularity_weight = apcTbl{9,2};
    F_granularity_weight = apcTbl{9,6};
    
    df_gap_weight = apcTbl{10,3};
    ss_gap_weight = apcTbl{10,2};
    F_gap_weight = apcTbl{10,6};
        
    ss_error = apcTbl{11, 2};
    df_error = apcTbl{11, 3};
    
    ss_total = apcTbl{12, 2};
        
    soa.omega2.apc.granularity = df_granularity * (F_granularity - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.gap = df_gap * (F_gap - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.weight = df_weight * (F_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.measure = df_measure * (F_measure - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.systems = df_systems * (F_systems - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.granularity_gap = df_granularity_gap * (F_granularity_gap - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.granularity_weight = df_granularity_weight * (F_granularity_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.apc.gap_weight = df_gap_weight * (F_gap_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
        
    soa.omega2p.apc.granularity = df_granularity * (F_granularity - 1) / (df_granularity * (F_granularity - 1) + N);
    soa.omega2p.apc.gap = df_gap * (F_gap - 1) / (df_gap * (F_gap - 1) + N);
    soa.omega2p.apc.weight = df_weight * (F_weight - 1) / (df_weight * (F_weight - 1) + N);
    soa.omega2p.apc.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.apc.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.apc.granularity_gap = df_granularity_gap * (F_granularity_gap - 1) / (df_granularity_gap * (F_granularity_gap - 1) + N);
    soa.omega2p.apc.granularity_weight = df_granularity_weight * (F_granularity_weight - 1) / (df_granularity_weight * (F_granularity_weight - 1) + N);
    soa.omega2p.apc.gap_weight = df_gap_weight * (F_gap_weight - 1) / (df_gap_weight * (F_gap_weight - 1) + N);
    
    
    soa.eta2.apc.granularity = ss_granularity / ss_total;
    soa.eta2.apc.gap = ss_gap / ss_total;
    soa.eta2.apc.weight = ss_weight / ss_total;
    soa.eta2.apc.measure = ss_measure / ss_total;
    soa.eta2.apc.systems = ss_systems / ss_total;
    soa.eta2.apc.granularity_gap = ss_granularity_gap / ss_total;
    soa.eta2.apc.granularity_weight = ss_granularity_weight / ss_total;
    soa.eta2.apc.gap_weight = ss_gap_weight / ss_total;
    
    
    soa.eta2p.apc.granularity = ss_granularity / (ss_granularity + ss_error);
    soa.eta2p.apc.gap = ss_gap / (ss_gap + ss_error);
    soa.eta2p.apc.weight = ss_weight / (ss_weight + ss_error);
    soa.eta2p.apc.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.apc.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2.apc.granularity_gap = ss_granularity_gap / (ss_granularity_gap + ss_error);
    soa.eta2.apc.granularity_weight = ss_granularity_weight / (ss_granularity_weight + ss_error);
    soa.eta2.apc.gap_weight = ss_gap_weight / (ss_gap_weight + ss_error);
    
    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    Dlabels = unique(factorD, 'stable');
    Elabels = unique(factorE, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.apc(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.apc(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.apc(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.apc(:), factorC(:), {'mean', 'meanci'});
    [me.factorD.mean, me.factorD.ci] = grpstats(data.apc(:), factorD(:), {'mean', 'meanci'});
    [me.factorE.mean, me.factorE.ci] = grpstats(data.apc(:), factorE(:), {'mean', 'meanci'});
    
    
    % interaction between granularity (x-axis) and gap (y-axis)
    % each row is a gap, columns are granularity
    ie.factorAB.mean = grpstats(data.apc(:), {factorA(:),factorB(:)}, {'mean'});
    ie.factorAB.mean = reshape(ie.factorAB.mean, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.granularity.number).';
    
    
    % interaction between granularity (x-axis) and weight (y-axis)
    % each row is a weight, columns are granularity
    ie.factorAC.mean = grpstats(data.apc(:), {factorA(:), factorC(:)}, {'mean'});
    ie.factorAC.mean = reshape(ie.factorAC.mean, EXPERIMENT.taxonomy.weight.number, EXPERIMENT.taxonomy.granularity.number).';
        
    % interaction between weight (x-axis) and gap (y-axis)
    % each row is a gap, columns are weight
    ie.factorCB.mean = grpstats(data.apc(:), {factorC(:), factorB(:)}, {'mean'});
    ie.factorCB.mean = reshape(ie.factorCB.mean, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.weight.number).';    
    
    
    yMin = Inf;
    yMax = -Inf;
    
    currentFigure = figure('Visible', 'off');
     
    subplot(1, 3, 1);
    
        xTick = 1:EXPERIMENT.taxonomy.granularity.number;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = xTick;
        ax1.XTickLabel = Alabels;
        %ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Granularity';
        
        %ax1.XGrid = 'on';
        
        %ax1.YLabel.Interpreter = 'tex';
        %ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));
    
   subplot(1, 3, 2);
    
        xTick = 1:EXPERIMENT.taxonomy.gap.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = xTick;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Gap';
                
        
        %ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));        
        
    subplot(1, 3, 3);
    
        xTick = 1:EXPERIMENT.taxonomy.weight.number;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = xTick;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Weight';
        
        
        %ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('AP Correlation Marginal Mean');
               
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            

    
        ax1.YLim = [yMin yMax];
       
        ax2.YLim = [yMin yMax];
    
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [82 22];
        currentFigure.PaperPosition = [1 1 80 20];
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'mainEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;
            
    
    % keeps the handles of the figures generated by multcompare, which
    % will be then put into subplots in the final figure
    mcFigureHandles = cell(1, 3);
    mcAxesHandles = cell(1, 3);
    
    % upper and lower limits for the axes
    mcLimits.lower = NaN(1, 3);
    mcLimits.upper = NaN(1, 3);
    
    
    mcFigureHandles{1} = figure('Visible', 'off');
    
    % comparing granularity
    [c, d, ~, nms] = multcompare(apcStats,'ctype', 'hsd', 'Dimension', [2], 'alpha', 0.05);
                    
    ax = gca;
    mcAxesHandles{1} = ax;
                
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
    
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('AP Correlation Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
        
    ax.YLabel.String = 'Granularity';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.granularity.list(end:-1:1);
            
    highlightTopGroup(mcFigureHandles{1}, d, true, EXPERIMENT.taxonomy.granularity.number, EXPERIMENT.taxonomy.granularity.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(1) = ax.XLim(1);
    mcLimits.upper(1) = ax.XLim(2);
    
    
    mcFigureHandles{2} = figure('Visible', 'off');
    
    % comparing gap
    [c, d, ~, nms] = multcompare(apcStats,'ctype', 'hsd', 'Dimension', [3], 'alpha', 0.05);
    
    ax = gca;
    mcAxesHandles{2} = ax;
    
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
        
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('AP Correlation Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
    
    ax.YLabel.String = 'Gap';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.gap.list(end:-1:1);
    
    highlightTopGroup(mcFigureHandles{2}, d, true, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.gap.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(2) = ax.XLim(1);
    mcLimits.upper(2) = ax.XLim(2);    
    
    mcFigureHandles{3} = figure('Visible', 'off');
    
    % comparing weight
    [c, d, ~, nms] = multcompare(apcStats,'ctype', 'hsd', 'Dimension', [4], 'alpha', 0.05);
    
    ax = gca;
    mcAxesHandles{3} = ax;
    
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
        
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('AP Correlation Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
    
    ax.YLabel.String = 'Weight';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.weight.list(end:-1:1);
    
    highlightTopGroup(mcFigureHandles{3}, d, true, EXPERIMENT.taxonomy.weight.number, EXPERIMENT.taxonomy.weight.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(3) = ax.XLim(1);
    mcLimits.upper(3) = ax.XLim(2);        
    
           
    currentFigure = figure('Visible', 'off');
    
    xLim = [min(mcLimits.lower) max(mcLimits.upper)]; 
    
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{1}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 1, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{1});
    
    
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{2}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 2, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{2});
             
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{3}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 3, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{3});    
    
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [82 22];
    currentFigure.PaperPosition = [1 1 80 20];
        
    figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'multcompare', shortTrackID);

    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
    close(currentFigure)
               
        
    
    yMin = Inf;
    yMax = -Inf;                 
        
    currentFigure = figure('Visible', 'off');
        
   subplot(1, 3, 1);
    
        plotData = ie.factorAB.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.gap.number 
            plot(1:EXPERIMENT.taxonomy.granularity.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.gap.color(l, :), 'LineWidth', 1);                
        end;
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = 1:EXPERIMENT.taxonomy.granularity.number;
        ax1.XTickLabel = Alabels;
        %ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Granularity';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        %ax1.XGrid = 'on';
        
        title('Granulary*Gap Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Blabels, 'Location', 'SouthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));  
    
    subplot(1, 3, 2);
    
        plotData = ie.factorAC.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.weight.number
            plot(1:EXPERIMENT.taxonomy.granularity.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.weight.color(l, :), 'LineWidth', 1);                
        end;
            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = 1:EXPERIMENT.taxonomy.granularity.number;
        ax2.XTickLabel = Alabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Granularity';
        
        ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        title('Granularity*Weight Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Clabels, 'Location', 'SouthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));    
        
    subplot(1, 3, 3);
    
        plotData = ie.factorCB.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.gap.number          
            plot(1:EXPERIMENT.taxonomy.weight.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.gap.color(l, :), 'LineWidth', 0.9);                  
        end;
            
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = 1:EXPERIMENT.taxonomy.weight.number;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Weight';
        
        ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('AP Correlation Marginal Mean');
        
        title('Weight*Gap Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Blabels, 'Location', 'SouthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            
    
        
        ax1.YLim = [yMin yMax];
        ax2.YLim = [yMin yMax];
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [82 22];
        currentFigure.PaperPosition = [1 1 80 20];  
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'apc', 'interactionEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;    
    
    
     % compute a 6-way ANOVA for RMSE
    [~, rmseTbl, rmseStats] = anovan(data.rmse, {subject, factorA, factorB, factorC, factorD, factorE}, 'Model', m, ...
        'VarNames', {'Kuple', 'Granularity', 'Gap', 'Weight', 'Measure', 'Systems'}, ...
        'alpha', 0.05, 'display', 'off');
    

   df_granularity = rmseTbl{3,3};
    ss_granularity = rmseTbl{3,2};
    F_granularity = rmseTbl{3,6};
    
    df_gap = rmseTbl{4,3};
    ss_gap = rmseTbl{4,2};
    F_gap = rmseTbl{4,6};
    
    df_weight = rmseTbl{5,3};
    ss_weight = rmseTbl{5,2};
    F_weight = rmseTbl{5,6};
    
    df_measure = rmseTbl{6,3};
    ss_measure = rmseTbl{6,2};
    F_measure = rmseTbl{6,6};
    
    df_systems = rmseTbl{7,3};
    ss_systems = rmseTbl{7,2};
    F_systems = rmseTbl{7,6};
    
    df_granularity_gap = rmseTbl{8,3};
    ss_granularity_gap = rmseTbl{8,2};
    F_granularity_gap = rmseTbl{8,6};
    
    df_granularity_weight = rmseTbl{9,3};
    ss_granularity_weight = rmseTbl{9,2};
    F_granularity_weight = rmseTbl{9,6};
    
    df_gap_weight = rmseTbl{10,3};
    ss_gap_weight = rmseTbl{10,2};
    F_gap_weight = rmseTbl{10,6};
        
    ss_error = rmseTbl{11, 2};
    df_error = rmseTbl{11, 3};
    
    ss_total = rmseTbl{12, 2};
        
    soa.omega2.rmse.granularity = df_granularity * (F_granularity - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.gap = df_gap * (F_gap - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.weight = df_weight * (F_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.measure = df_measure * (F_measure - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.systems = df_systems * (F_systems - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.granularity_gap = df_granularity_gap * (F_granularity_gap - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.granularity_weight = df_granularity_weight * (F_granularity_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
    soa.omega2.rmse.gap_weight = df_gap_weight * (F_gap_weight - 1) / (df_granularity * F_granularity + df_gap * F_gap + df_weight * F_weight + df_measure * F_measure + df_systems * F_systems + df_granularity_gap * F_granularity_gap + df_granularity_weight * F_granularity_weight + df_gap_weight * F_gap_weight + df_error + 1);
        
    soa.omega2p.rmse.granularity = df_granularity * (F_granularity - 1) / (df_granularity * (F_granularity - 1) + N);
    soa.omega2p.rmse.gap = df_gap * (F_gap - 1) / (df_gap * (F_gap - 1) + N);
    soa.omega2p.rmse.weight = df_weight * (F_weight - 1) / (df_weight * (F_weight - 1) + N);
    soa.omega2p.rmse.measure = df_measure * (F_measure - 1) / (df_measure * (F_measure - 1) + N);
    soa.omega2p.rmse.systems = df_systems * (F_systems - 1) / (df_systems * (F_systems - 1) + N);
    soa.omega2p.rmse.granularity_gap = df_granularity_gap * (F_granularity_gap - 1) / (df_granularity_gap * (F_granularity_gap - 1) + N);
    soa.omega2p.rmse.granularity_weight = df_granularity_weight * (F_granularity_weight - 1) / (df_granularity_weight * (F_granularity_weight - 1) + N);
    soa.omega2p.rmse.gap_weight = df_gap_weight * (F_gap_weight - 1) / (df_gap_weight * (F_gap_weight - 1) + N);
    
    
    soa.eta2.rmse.granularity = ss_granularity / ss_total;
    soa.eta2.rmse.gap = ss_gap / ss_total;
    soa.eta2.rmse.weight = ss_weight / ss_total;
    soa.eta2.rmse.measure = ss_measure / ss_total;
    soa.eta2.rmse.systems = ss_systems / ss_total;
    soa.eta2.rmse.granularity_gap = ss_granularity_gap / ss_total;
    soa.eta2.rmse.granularity_weight = ss_granularity_weight / ss_total;
    soa.eta2.rmse.gap_weight = ss_gap_weight / ss_total;
    
    
    soa.eta2p.rmse.granularity = ss_granularity / (ss_granularity + ss_error);
    soa.eta2p.rmse.gap = ss_gap / (ss_gap + ss_error);
    soa.eta2p.rmse.weight = ss_weight / (ss_weight + ss_error);
    soa.eta2p.rmse.measure = ss_measure / (ss_measure + ss_error);
    soa.eta2p.rmse.systems = ss_systems / (ss_systems + ss_error);
    soa.eta2.rmse.granularity_gap = ss_granularity_gap / (ss_granularity_gap + ss_error);
    soa.eta2.rmse.granularity_weight = ss_granularity_weight / (ss_granularity_weight + ss_error);
    soa.eta2.rmse.gap_weight = ss_gap_weight / (ss_gap_weight + ss_error);
    
    Slabels = unique(subject, 'stable');
    Alabels = unique(factorA, 'stable');
    Blabels = unique(factorB, 'stable');
    Clabels = unique(factorC, 'stable');
    Dlabels = unique(factorD, 'stable');
    Elabels = unique(factorE, 'stable');
    
    % main effects
    [me.subject.mean, me.subject.ci] = grpstats(data.rmse(:), subject(:), {'mean', 'meanci'});
    [me.factorA.mean, me.factorA.ci] = grpstats(data.rmse(:), factorA(:), {'mean', 'meanci'});
    [me.factorB.mean, me.factorB.ci] = grpstats(data.rmse(:), factorB(:), {'mean', 'meanci'});
    [me.factorC.mean, me.factorC.ci] = grpstats(data.rmse(:), factorC(:), {'mean', 'meanci'});
    [me.factorD.mean, me.factorD.ci] = grpstats(data.rmse(:), factorD(:), {'mean', 'meanci'});
    [me.factorE.mean, me.factorE.ci] = grpstats(data.rmse(:), factorE(:), {'mean', 'meanci'});
    
    
    % interaction between granularity (x-axis) and gap (y-axis)
    % each row is a gap, columns are granularity
    ie.factorAB.mean = grpstats(data.rmse(:), {factorA(:),factorB(:)}, {'mean'});
    ie.factorAB.mean = reshape(ie.factorAB.mean, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.granularity.number).';
    
    
    % interaction between granularity (x-axis) and weight (y-axis)
    % each row is a weight, columns are granularity
    ie.factorAC.mean = grpstats(data.rmse(:), {factorA(:), factorC(:)}, {'mean'});
    ie.factorAC.mean = reshape(ie.factorAC.mean, EXPERIMENT.taxonomy.weight.number, EXPERIMENT.taxonomy.granularity.number).';
        
    % interaction between weight (x-axis) and gap (y-axis)
    % each row is a gap, columns are weight
    ie.factorCB.mean = grpstats(data.rmse(:), {factorC(:), factorB(:)}, {'mean'});
    ie.factorCB.mean = reshape(ie.factorCB.mean, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.weight.number).';    
    
    
    yMin = Inf;
    yMax = -Inf;
    
    currentFigure = figure('Visible', 'off');
     
    subplot(1, 3, 1);
    
        xTick = 1:EXPERIMENT.taxonomy.granularity.number;
        plotData.mean = me.factorA.mean.';
        plotData.ciLow = me.factorA.ci(:, 1).';
        plotData.ciHigh = me.factorA.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = xTick;
        ax1.XTickLabel = Alabels;
        %ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Granularity';
        
        %ax1.XGrid = 'on';
        
        %ax1.YLabel.Interpreter = 'tex';
        %ax1.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));
    
   subplot(1, 3, 2);
    
        xTick = 1:EXPERIMENT.taxonomy.gap.number;
        plotData.mean = me.factorB.mean.';
        plotData.ciLow = me.factorB.ci(:, 1).';
        plotData.ciHigh = me.factorB.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = xTick;
        ax2.XTickLabel = Blabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Gap';
                
        
        %ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));        
        
    subplot(1, 3, 3);
    
        xTick = 1:EXPERIMENT.taxonomy.weight.number;
        plotData.mean = me.factorC.mean.';
        plotData.ciLow = me.factorC.ci(:, 1).';
        plotData.ciHigh = me.factorC.ci(:, 2).';        
    
        plot(xTick, plotData.mean, 'Color', rgb('Black'), 'LineWidth', 1.5);
        
        hold on
        
        hFill = fill([xTick fliplr(xTick)],[plotData.ciHigh fliplr(plotData.ciLow)], rgb('Black'), ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.10, 'FaceAlpha', 0.10);

        % send the fill to back
        uistack(hFill, 'bottom');
            
        % Exclude fill from legend
		set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
    
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = xTick;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Weight';
        
        
        %ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
               
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            

    
        ax1.YLim = [yMin yMax];
       
        ax2.YLim = [yMin yMax];
    
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [82 22];
        currentFigure.PaperPosition = [1 1 80 20];
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'mainEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;
            
    
    % keeps the handles of the figures generated by multcompare, which
    % will be then put into subplots in the final figure
    mcFigureHandles = cell(1, 3);
    mcAxesHandles = cell(1, 3);
    
    % upper and lower limits for the axes
    mcLimits.lower = NaN(1, 3);
    mcLimits.upper = NaN(1, 3);
    
    
    mcFigureHandles{1} = figure('Visible', 'off');
    
    % comparing granularity
    [c, d, ~, nms] = multcompare(rmseStats,'ctype', 'hsd', 'Dimension', [2], 'alpha', 0.05);
                    
    ax = gca;
    mcAxesHandles{1} = ax;
                
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
    
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('Normalized RMSE Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
        
    ax.YLabel.String = 'Granularity';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.granularity.list(end:-1:1);
            
    highlightTopGroup(mcFigureHandles{1}, d, false, EXPERIMENT.taxonomy.granularity.number, EXPERIMENT.taxonomy.granularity.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(1) = ax.XLim(1);
    mcLimits.upper(1) = ax.XLim(2);
    
    
    mcFigureHandles{2} = figure('Visible', 'off');
    
    % comparing gap
    [c, d, ~, nms] = multcompare(rmseStats,'ctype', 'hsd', 'Dimension', [3], 'alpha', 0.05);
    
    ax = gca;
    mcAxesHandles{2} = ax;
    
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
        
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('Normalized RMSE Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
    
    ax.YLabel.String = 'Gap';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.gap.list(end:-1:1);
    
    highlightTopGroup(mcFigureHandles{2}, d, false, EXPERIMENT.taxonomy.gap.number, EXPERIMENT.taxonomy.gap.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(2) = ax.XLim(1);
    mcLimits.upper(2) = ax.XLim(2);    
    
    mcFigureHandles{3} = figure('Visible', 'off');
    
    % comparing weight
    [c, d, ~, nms] = multcompare(rmseStats,'ctype', 'hsd', 'Dimension', [4], 'alpha', 0.05);
    
    ax = gca;
    mcAxesHandles{3} = ax;
    
    ax.Title.String = '';
    ax.Title.Interpreter = 'tex';
        
    ax.TickLabelInterpreter = 'tex';
    
    ax.XLabel.String = sprintf('Normalized RMSE Marginal Mean');
    ax.XLabel.Interpreter = 'tex';
    
    ax.YLabel.String = 'Weight';
    ax.YLabel.Interpreter = 'tex';
    
    ax.YTickLabel = EXPERIMENT.taxonomy.weight.list(end:-1:1);
    
    highlightTopGroup(mcFigureHandles{3}, d, false, EXPERIMENT.taxonomy.weight.number, EXPERIMENT.taxonomy.weight.color);
    
    ax.YGrid = 'on';
    
    mcLimits.lower(3) = ax.XLim(1);
    mcLimits.upper(3) = ax.XLim(2);        
    
           
    currentFigure = figure('Visible', 'off');
    
    xLim = [min(mcLimits.lower) max(mcLimits.upper)]; 
    
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{1}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 1, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{1});
    
    
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{2}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 2, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{2});
             
    % copy the plot generated by multcompare into current
    % figure
    ax = copyobj(mcAxesHandles{3}, currentFigure);
    ax.XLim = xLim;
    ax.FontSize = 24;
    %ax.Title.FontSize = 72;
    %ax.XLabel.FontSize = 72;
    %ax.YLabel.FontSize = 72;
    
    % move it to the appropriate subplot
    subplot(1, 3, 3, ax);
    
    % close the plot generated by multcompare
    close(mcFigureHandles{3});    
    
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [82 22];
    currentFigure.PaperPosition = [1 1 80 20];
        
    figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'multcompare', shortTrackID);

    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
    close(currentFigure)
               
        
    
    yMin = Inf;
    yMax = -Inf;                 
        
    currentFigure = figure('Visible', 'off');
        
   subplot(1, 3, 1);
    
        plotData = ie.factorAB.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.gap.number 
            plot(1:EXPERIMENT.taxonomy.granularity.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.gap.color(l, :), 'LineWidth', 1);                
        end;
            
        ax1 = gca;
        ax1.TickLabelInterpreter = 'tex';
        ax1.FontSize = 24;
        
        ax1.XTick = 1:EXPERIMENT.taxonomy.granularity.number;
        ax1.XTickLabel = Alabels;
        %ax1.XTickLabelRotation = 90;
        
        ax1.XLabel.Interpreter = 'tex';
        ax1.XLabel.String = 'Granularity';
        
        ax1.YLabel.Interpreter = 'tex';
        ax1.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        %ax1.XGrid = 'on';
        
        title('Granulary*Gap Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Blabels, 'Location', 'NorthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax1.YLim(1));
        yMax = max(yMax, ax1.YLim(2));  
    
    subplot(1, 3, 2);
    
        plotData = ie.factorAC.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.weight.number
            plot(1:EXPERIMENT.taxonomy.granularity.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.weight.color(l, :), 'LineWidth', 1);                
        end;
            
        ax2 = gca;
        ax2.TickLabelInterpreter = 'tex';
        ax2.FontSize = 24;
        
        ax2.XTick = 1:EXPERIMENT.taxonomy.granularity.number;
        ax2.XTickLabel = Alabels;
        %ax2.XTickLabelRotation = 90;
        
        ax2.XLabel.Interpreter = 'tex';
        ax2.XLabel.String = 'Granularity';
        
        ax2.YLabel.Interpreter = 'tex';
        %ax2.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        title('Granularity*Weight Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Clabels, 'Location', 'NorthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax2.YLim(1));
        yMax = max(yMax, ax2.YLim(2));    
        
    subplot(1, 3, 3);
    
        plotData = ie.factorCB.mean.';
        
        
        hold on;
        % for each label
        for l = 1:EXPERIMENT.taxonomy.gap.number          
            plot(1:EXPERIMENT.taxonomy.weight.number, plotData(l, :), 'Color', EXPERIMENT.taxonomy.gap.color(l, :), 'LineWidth', 0.9);                  
        end;
            
        ax3 = gca;
        ax3.TickLabelInterpreter = 'tex';
        ax3.FontSize = 24;
        
        ax3.XTick = 1:EXPERIMENT.taxonomy.weight.number;
        ax3.XTickLabel = Clabels;
        %ax3.XTickLabelRotation = 90;
        
        ax3.XLabel.Interpreter = 'tex';
        ax3.XLabel.String = 'Weight';
        
        ax3.YLabel.Interpreter = 'tex';
        %ax3.YLabel.String = sprintf('Normalized RMSE Marginal Mean');
        
        title('Weight*Gap Interaction', 'FontSize', 24, 'Interpreter', 'tex');
        
        legend(Blabels, 'Location', 'NorthEast', 'Interpreter', 'none')
        
        yMin = min(yMin, ax3.YLim(1));
        yMax = max(yMax, ax3.YLim(2));            
    
        
        ax1.YLim = [yMin yMax];
        ax2.YLim = [yMin yMax];
        ax3.YLim = [yMin yMax];
        
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [82 22];
        currentFigure.PaperPosition = [1 1 80 20];  
        
        figureID = EXPERIMENT.pattern.identifier.anova.figure(factors, 'rmse', 'interactionEffects', shortTrackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, factors, figureID));
        
        close(currentFigure)
        
        clear plotData;    
        
    
    
        
        
    anovaID = EXPERIMENT.pattern.identifier.anova.id(factors, shortTrackID);
    
    sersave(EXPERIMENT.pattern.file.anova(trackID, factors, anovaID), 'apcTbl', 'apcStats', 'rmseTbl', 'rmseStats', 'soa');
                
    fprintf('\n\n######## Total elapsed time for computing %s ANOVA analyses on collection %s (%s): %s ########\n\n', ...
            upper(factors), EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end


function [] =  highlightTopGroup(h, m, usemax, samples, colors)

    common_parameters;
    
    % the index of the top element in the top group
    if usemax
        [~, topElement] = max(m(:, 1));
    else
        [~, topElement] = min(m(:, 1));
    end;
    
    % Get bounds for the top group
    topGroupHandle = findobj(h, 'UserData', -topElement);
    if (isempty(topGroupHandle))
        return; 
    end % unexpected

    topGroupX = get(topGroupHandle, 'XData');
    topGroupLower = min(topGroupX);
    topGroupUpper = max(topGroupX);
    
    % Change the comparison lines to use these values
    comparisonLines = findobj(h, 'UserData', 'Comparison lines');
    if (~isempty(comparisonLines))
       comparisonLines.LineWidth = 2;
       comparisonLines.XData(1:2) = topGroupLower;
       comparisonLines.XData(4:5) = topGroupUpper;
    end

    % arrange line styles and markers
    for e=1:samples

        % look for the line and marker of the current element
        lineHandle = findobj(h, 'UserData', -e, 'Type','Line', '-and', 'LineStyle', '-', '-and', 'Marker', 'none');
        markerHandle = findobj(h, 'UserData', e, 'Type','Line', '-and', 'LineStyle', 'none', '-and', 'Marker', 'o');

        if (isempty(lineHandle))
            continue;
        end  % unexpected

        currentElementX = get(lineHandle, 'XData');
        currentElementLower = min(currentElementX);
        currentElementUpper = max(currentElementX);
        
        % To be in the top group the upper bound of the current element
        % (CEL) must be above the lower bound of the top group (TGL) and
        % the lower bound of the current element (CEL) must be below the
        % upper bound of the top groud (TGL)
        %
        %  CEL    TGL   CEU  TGU
        %   |      |_____|____|
        %   |____________|
        %
        %  TGL   CEL  TGU     CEU
        %   |_____|____|       |
        %         |____________|
        %
        if ( currentElementUpper > topGroupLower && currentElementLower < topGroupUpper)
            lineHandle.Color = colors(e, :);
            lineHandle.LineWidth = 2.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = colors(e, :);
            markerHandle.MarkerEdgeColor = colors(e, :);
            markerHandle.MarkerSize = 10;
        else
            lineHandle.Color = 'k';
            lineHandle.LineWidth = 1.5;
            lineHandle.Visible = 'on';
            markerHandle.MarkerFaceColor = 'k';
            markerHandle.MarkerEdgeColor = 'k';
            markerHandle.MarkerSize = 6;
        end;
        
    end; % for tags

 
end



