%% randomPool
% 
% Generates a random pool from the template one.

%% Synopsis
%
%   [rndPool] = randomPool(identifier, template)
%  
%
%   It assumes binary relevance, i.e. only NotRelevant and Relevant degrees
%   are allowed.
%
%
% *Parameters*
%
% * *|identifier|* - the identifier to be assigned to the random pool;
% * *|template|* - a template pool to be randomized;
% * *|props|* - the proportion of documents in each relavance degree.
%
% *Returns*
%
% * |rndPool|  - the random pool.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [rndPool] = randomPool(identifier, template, props)

     % check that props is a nonempty scalar "real" value
    % greater than 0 and less than or equal to 1
    validateattributes(props, {'numeric'}, ...
        {'nonempty', 'vector', 'real', '>', 0, '<', 1}, '', 'props');

    % check that identifier is a non-empty string
    validateattributes(identifier,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'Identifier');

     if iscell(identifier)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(identifier) && numel(identifier) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    identifier = char(strtrim(identifier));
    identifier = identifier(:).';

    % check that the identifier is ok according to the matlab rules
    if ~isempty(regexp(identifier, '(^[0-9_])?\W*', 'once'))
        error('MATTERS:IllegalArgument', 'Identifier %s is not valid: identifiers can contain only letters, numbers, and the underscore character and they must start with a letter.', ...
            identifier);
    end  

    
    % the possible relevance degrees
    relevanceDegrees = categories(template{:, 1}{1, 1}.RelevanceDegree);
    relevanceDegrees = relevanceDegrees(:);
    degrees = length(relevanceDegrees);
    revelanceDegreesRange = [1, length(relevanceDegrees)];
    
    assert(length(props) == length(relevanceDegrees), 'The number of elements in props must be the same as the number of relevance degrees in the pool');
    
    % a vector of thresholds
    thresholds = [0 cumsum(props(:).')];
    

    % the random pool has the same (topic, document) pairs of the template
    % pool but randomized labels
    rndPool = template;
    rndPool.Properties.UserData.identifier = identifier;
    rndPool.Properties.VariableNames = {identifier};
    
    for t = 1:height(template);
        
        % generate as many uniform random numbers as assessed documents for
        % the topic
        x = rand(height(rndPool{t, 1}{1, 1}), 1);
        
        ranges = cell(1, degrees);
        
        % find where x falls within the requested thresholds
        for r = 1:degrees            
            ranges{r} = (x >= thresholds(r)) & (x < thresholds(r+1));
        end;
        
        % turn ranges into indexs into the relevanceDegrees vector
        for r = 1:degrees
            x(ranges{r}) = r;
        end;
        
        rndPool{t, 1}{1, 1}{:, 2} = relevanceDegrees(x);
        
    end; % for each topic

end

