%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


diary off;


%% General configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
end;

% The base path
if (strcmpi(computer, 'GLNXA64'))
    % if we are running on the cluster 
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/TOIS2016-FFM/experiment/';
else
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2016/TOIS2016-FFM/experiment/';
end;

% The path for logs
EXPERIMENT.path.log = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'log', filesep);

% The path for datasets
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);


% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'TOIS 2016 FFM';

% Label to be used for the gold standard pool
EXPERIMENT.label.goldPool = 'GOLD';


%% Overall Experiment Taxonomy

EXPERIMENT.taxonomy.aw.id = 'aw';
EXPERIMENT.taxonomy.aw.level = 'family';
EXPERIMENT.taxonomy.aw.description = 'AWARE family of measures';
EXPERIMENT.taxonomy.aw.children = {'us'};

EXPERIMENT.taxonomy.us.id = 'us';
EXPERIMENT.taxonomy.us.level = 'learning';
EXPERIMENT.taxonomy.us.description = 'Unsupervised approaches';
EXPERIMENT.taxonomy.us.children = {'mono'};

EXPERIMENT.taxonomy.mono.id = 'mono';
EXPERIMENT.taxonomy.mono.level = 'feature';
EXPERIMENT.taxonomy.mono.description = 'Mono-feature approaches';
EXPERIMENT.taxonomy.mono.children = {'sgl', 'tpc'};

EXPERIMENT.taxonomy.sgl.id = 'sgl';
EXPERIMENT.taxonomy.sgl.level = 'granularity';
EXPERIMENT.taxonomy.sgl.description = 'Single coefficient for each assessor';
EXPERIMENT.taxonomy.sgl.color = [0 16 178];
EXPERIMENT.taxonomy.sgl.children = {'msr', 'gp', 'wgt'};

EXPERIMENT.taxonomy.tpc.id = 'tpc';
EXPERIMENT.taxonomy.tpc.level = 'granularity';
EXPERIMENT.taxonomy.tpc.description = 'Topic-by-topic coefficient for each assessor';
EXPERIMENT.taxonomy.tpc.color = [255 200 0];
EXPERIMENT.taxonomy.tpc.children = {'msr', 'gp', 'wgt'};

EXPERIMENT.taxonomy.msr.id = 'msr';
EXPERIMENT.taxonomy.msr.level = 'aggregation';
EXPERIMENT.taxonomy.msr.description = 'Average performed on the random assessor measures';
EXPERIMENT.taxonomy.msr.color = [67 200 6];
EXPERIMENT.taxonomy.msr.children = {'fro', 'rmse', 'kld', 'apc', 'tau'};

EXPERIMENT.taxonomy.gp.id = 'gp';
EXPERIMENT.taxonomy.gp.level = 'aggregation';
EXPERIMENT.taxonomy.gp.description = 'Average performed on the gaps with random assessors';
EXPERIMENT.taxonomy.gp.color = [9 11 178];
EXPERIMENT.taxonomy.gp.children = {'fro', 'rmse', 'kld', 'apc', 'tau'};

EXPERIMENT.taxonomy.wgt.id = 'wgt';
EXPERIMENT.taxonomy.wgt.level = 'aggregation';
EXPERIMENT.taxonomy.wgt.description = 'Average performed on the weights with random assessors';
EXPERIMENT.taxonomy.wgt.color = [255 85 0];
EXPERIMENT.taxonomy.wgt.children = {'fro', 'rmse', 'kld', 'apc', 'tau'};

EXPERIMENT.taxonomy.fro.id = 'fro';
EXPERIMENT.taxonomy.fro.level = 'gap';
EXPERIMENT.taxonomy.fro.description = 'Frobenius norm as gap with random assessors';
EXPERIMENT.taxonomy.fro.color = [178 42 0];
EXPERIMENT.taxonomy.fro.children = {'md', 'msd', 'med'};

EXPERIMENT.taxonomy.rmse.id = 'rmse';
EXPERIMENT.taxonomy.rmse.level = 'gap';
EXPERIMENT.taxonomy.rmse.description = 'Root mean squared error as gap with random assessors';
EXPERIMENT.taxonomy.rmse.color = [255 221 13];
EXPERIMENT.taxonomy.rmse.children = {'md', 'msd', 'med'};

EXPERIMENT.taxonomy.kld.id = 'kld';
EXPERIMENT.taxonomy.kld.level = 'gap';
EXPERIMENT.taxonomy.kld.description = 'Kullback-Liebler divergence as gap with random assessors';
EXPERIMENT.taxonomy.kld.color = [5 255 4];
EXPERIMENT.taxonomy.kld.children = {'md', 'msd', 'med'};

EXPERIMENT.taxonomy.apc.id = 'apc';
EXPERIMENT.taxonomy.apc.level = 'gap';
EXPERIMENT.taxonomy.apc.description = 'AP correlation as gap with random assessors';
EXPERIMENT.taxonomy.apc.color = [0 119 178];
EXPERIMENT.taxonomy.apc.children = {'md', 'msd', 'med'};

EXPERIMENT.taxonomy.tau.id = 'tau';
EXPERIMENT.taxonomy.tau.description = 'Kendall''s tau as gap with random assessors';
EXPERIMENT.taxonomy.tau.color = [236 12 255];
EXPERIMENT.taxonomy.tau.children = {'md', 'msd', 'med'};

EXPERIMENT.taxonomy.md.id = 'md';
EXPERIMENT.taxonomy.md.level = 'weight';
EXPERIMENT.taxonomy.md.description = 'Minimal dissimilarity from random assessors';
EXPERIMENT.taxonomy.md.color = [255 138 38];

EXPERIMENT.taxonomy.msd.id = 'msd';
EXPERIMENT.taxonomy.msd.level = 'weight';
EXPERIMENT.taxonomy.msd.description = 'Minimal squared dissimilarity from random assessors';
EXPERIMENT.taxonomy.msd.color = [67 12 255];

EXPERIMENT.taxonomy.med.id = 'med';
EXPERIMENT.taxonomy.med.level = 'weight';
EXPERIMENT.taxonomy.med.description = 'Maximal equi-dissimilarity from random assessors';
EXPERIMENT.taxonomy.med.color = [27 178 33];

% The baseline approaches to confront with
EXPERIMENT.taxonomy.baseline.id = 'baseline';
EXPERIMENT.taxonomy.baseline.description = 'Baseline approaches';

% Command to create the tag of an experiment from its components
EXPERIMENT.taxonomy.tag = @(family, learning, feature, granularity, aggregation, gap, weight) ...
    sprintf('%1$s_%2$s_%3$s_%4$s_%5$s_%6$s_%7$s', ...
        family, learning, feature, granularity, aggregation, gap, weight);

% The root of the taxonomy
EXPERIMENT.taxonomy.root.id = EXPERIMENT.taxonomy.aw.id;
EXPERIMENT.taxonomy.root.description = 'The root of the experimental tree';


granularity.list = {'sgl', 'tpc'};
granularity.number = length(granularity.list);
    
%aggregation.list = {'msr', 'gp', 'wgt'};
aggregation.list = {'gp'};
aggregation.number = length(aggregation.list);
    
gap.list = {'fro', 'rmse', 'kld', 'apc', 'tau'};
gap.number = length(gap.list);
    
weight.list = {'md', 'msd', 'med'};
weight.number = length(weight.list);



%% Configuration for measures

% the identifier of the measures under experimentation
%EXPERIMENT.measure.id = {'ap', 'p10', 'rbp', 'ndcg20', 'err20'};
EXPERIMENT.measure.id = {'ap', 'ndcg20', 'err20'};
%EXPERIMENT.measure.id = {'ap'};

% the number of measures under experimentation
EXPERIMENT.measure.number = length(EXPERIMENT.measure.id);

% the names of the measures under experimentation
%EXPERIMENT.measure.name = {'AP', 'P@10', 'RBP', 'nDCG@20', 'ERR@20'};
EXPERIMENT.measure.name = {'AP', 'nDCG@20', 'ERR@20'};

% the full names of the measures under experimentation
%EXPERIMENT.measure.fullName = {'Average Precision (AP)', 'Precision at 10 Retrieved Documents (P10)', ...
%    'Rank-biased Precision (RBP)', 'Normalized Discounted Cumulated Gain at 20 Retrieved Documents (nDCG20)', ...
%    'Expected Reciprocal Rank at 20 Retrieved Document (ERR20)'};
EXPERIMENT.measure.fullName = {'Average Precision (AP)', ...
    'Normalized Discounted Cumulated Gain at 20 Retrieved Documents (nDCG20)', ...
    'Expected Reciprocal Rank at 20 Retrieved Document (ERR20)'};


%% Configuration for the kuples to be used

% Number of pools/assessors to be merged
EXPERIMENT.kuples.sizes = 2:30;

EXPERIMENT.kuples.labelNames = strtrim(cellstr(num2str(EXPERIMENT.kuples.sizes(:), 'k = %d')));

% The total number of merges to performa
EXPERIMENT.kuples.number = length(EXPERIMENT.kuples.sizes);

% Maximum number of k-uples to be sampled
EXPERIMENT.kuples.samples = 1000;


%% Configuration for analyses

% The confidence degree for computing the confidence interval
EXPERIMENT.analysis.ciAlpha = 0.05;

% The number of samples for ties in AP correlation
EXPERIMENT.analysis.apcorrTiesSamples = 100;

% The bandwidth for KDE
EXPERIMENT.analysis.kdeBandwidth = 0.015;

% The bins for kernel density estimation (exceed the range [0 1] to
% compensate for the bandwidth of KSD
EXPERIMENT.analysis.kdeBins = -0.1:0.01:1.1;

% The number of random pools to be generated
EXPERIMENT.analysis.rndPoolsSamples = 1000;


%% Configuration for the Expectation-Maximization algorithm

% Maximum number of iterations for expectation maximization algorithm
EXPERIMENT.em.maxIterations = 1000;

% Tolerance for expectation maximization algorithm
EXPERIMENT.em.tolerance = 1e-3;


%% Data for the different experiments

% Baseline data
EXPERIMENT.tag.base.id = 'base';
EXPERIMENT.tag.base.description = 'Baseline data';
EXPERIMENT.tag.base.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);

% Majority vote pools
EXPERIMENT.tag.mv.id = 'mv';
EXPERIMENT.tag.mv.description = 'Majority vote pools';
EXPERIMENT.tag.mv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.mv.latex.color = 'LemonChiffon2';
EXPERIMENT.tag.mv.latex.label = '\texttt{mv}';
EXPERIMENT.tag.mv.matlab.color = [.932,.9,.52];
EXPERIMENT.tag.mv.matlab.label = 'mv';

% Expectation-Maximization pools with random seeds
EXPERIMENT.tag.emrnd.id = 'emrnd';
EXPERIMENT.tag.emrnd.description = 'Expectation-Maximization pools with random seeds';
EXPERIMENT.tag.emrnd.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emrnd.command = @expectationMaximizationPoolRnd;
EXPERIMENT.tag.emrnd.latex.color = 'LemonChiffon1';
EXPERIMENT.tag.emrnd.latex.label = '\texttt{emrnd}';
EXPERIMENT.tag.emrnd.matlab.color = [1,.965,.56];
EXPERIMENT.tag.emrnd.matlab.label = 'emrnd';

% Expectation-Maximization pools seeded by majority vote
EXPERIMENT.tag.emmv.id = 'emmv';
EXPERIMENT.tag.emmv.description = 'Expectation-Maximization pools seeded by majority vote';
EXPERIMENT.tag.emmv.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emmv.command = @expectationMaximizationPoolMv;
EXPERIMENT.tag.emmv.latex.color = 'LemonChiffon1';
EXPERIMENT.tag.emmv.latex.label = '\texttt{emmv}';
EXPERIMENT.tag.emmv.matlab.color = [1,.965,.56];
EXPERIMENT.tag.emmv.matlab.label = 'emmv';

% Expectation-Maximization pools, North-Eastern University variant
EXPERIMENT.tag.emneu.id = 'emneu';
EXPERIMENT.tag.emneu.description = 'Expectation-Maximization pools, North-Eastern University variant';
EXPERIMENT.tag.emneu.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.emneu.command = @expectationMaximizationPoolNeu;
EXPERIMENT.tag.emneu.latex.color = 'LemonChiffon1';
EXPERIMENT.tag.emneu.latex.label = '\texttt{emneu}';
EXPERIMENT.tag.emneu.matlab.color = [1,.965,.56];
EXPERIMENT.tag.emneu.matlab.label = 'emneu';

% Random uniform pools with assessors judging 5% of the documents as relevant
EXPERIMENT.tag.rnd005.id = 'rnd005';
EXPERIMENT.tag.rnd005.description = 'Random uniform pools with assessors judging 5% of the documents as relevant';
EXPERIMENT.tag.rnd005.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd005.ratio = [0.95 0.05];

% Random uniform pools with assessors judging 10% of the documents as relevant
EXPERIMENT.tag.rnd010.id = 'rnd010';
EXPERIMENT.tag.rnd010.description = 'Random uniform pools with assessors judging 10% of the documents as relevant';
EXPERIMENT.tag.rnd010.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd010.ratio = [0.90 0.10];

% Random uniform pools with assessors judging 15% of the documents as relevant
EXPERIMENT.tag.rnd015.id = 'rnd015';
EXPERIMENT.tag.rnd015.description = 'Random uniform pools with assessors judging 15% of the documents as relevant';
EXPERIMENT.tag.rnd015.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd015.ratio = [0.85 0.15];

% Random uniform pools with assessors judging 20% of the documents as relevant
EXPERIMENT.tag.rnd020.id = 'rnd020';
EXPERIMENT.tag.rnd020.description = 'Random uniform pools with assessors judging 20% of the documents as relevant';
EXPERIMENT.tag.rnd020.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd020.ratio = [0.80 0.20];

% Random uniform pools with assessors judging 50% of the documents as relevant
EXPERIMENT.tag.rnd050.id = 'rnd050';
EXPERIMENT.tag.rnd050.description = 'Random uniform pools with assessors judging 50% of the documents as relevant';
EXPERIMENT.tag.rnd050.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd050.ratio = [0.50 0.50];

% Random uniform pools with assessors judging 80% of the documents as relevant
EXPERIMENT.tag.rnd080.id = 'rnd080';
EXPERIMENT.tag.rnd080.description = 'Random uniform pools with assessors judging 80% of the documents as relevant';
EXPERIMENT.tag.rnd080.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd080.ratio = [0.20 0.80];

% Random uniform pools with assessors judging 85% of the documents as relevant
EXPERIMENT.tag.rnd085.id = 'rnd085';
EXPERIMENT.tag.rnd085.description = 'Random uniform pools with assessors judging 85% of the documents as relevant';
EXPERIMENT.tag.rnd085.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd085.ratio = [0.15 0.85];

% Random uniform pools with assessors judging 90% of the documents as relevant
EXPERIMENT.tag.rnd090.id = 'rnd090';
EXPERIMENT.tag.rnd090.description = 'Random uniform pools with assessors judging 90% of the documents as relevant';
EXPERIMENT.tag.rnd090.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd090.ratio = [0.10 0.90];

% Random uniform pools with assessors judging 95% of the documents as relevant
EXPERIMENT.tag.rnd095.id = 'rnd095';
EXPERIMENT.tag.rnd095.description = 'Random uniform pools with assessors judging 95% of the documents as relevant';
EXPERIMENT.tag.rnd095.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.rnd095.ratio = [0.05 0.95];

% Baseline AWARE: uniform weights for all assessors
EXPERIMENT.tag.aw_uni.id = 'aw_uni';
EXPERIMENT.tag.aw_uni.relativePath = sprintf('%1$s%2$s', EXPERIMENT.taxonomy.baseline.id, filesep);
EXPERIMENT.tag.aw_uni.score.command = @aw_uni;
EXPERIMENT.tag.aw_uni.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_uni.latex.color = 'Ivory3';
EXPERIMENT.tag.aw_uni.latex.label = '\texttt{uni}';
EXPERIMENT.tag.aw_uni.matlab.color = [.624,.475,.932];
EXPERIMENT.tag.aw_uni.matlab.label = 'uni';

%>>>>>>>>>> HERE BECAUSE OF MATLAB PARSING
% Commands to be executed to map the different gaps in the range [0, 1] so
% that 0 means different from random assessor and 1 means equal to random
% assessor

% Frobenius norm is in [0, sqrt(T*R)], where 0 means equal to random
% assessor. Divide it by its max and reverse it so that 0 means different
% from random assessor
EXPERIMENT.command.normalize.fro = @(x, T, R) 1 - (x./sqrt(T*R));

% RMSE is in [0, 1] where 0 means equal to random assessor. Reverse it so 
% that 0 means different from random assessor
EXPERIMENT.command.normalize.rmse = @(x, T, R) 1 - x;

% AP correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.apc = @(x, T, R) abs(x);

% Kendall's tau correlation is in [-1, 1], where 0 means different from random
% assessor, 1 means equal to random assessor and -1 completely opposite to
% random assessor. Consider -1 as 1. 
EXPERIMENT.command.normalize.tau = @(x, T, R) abs(x);

% KL divergence is in [0, +\infty), where 0 means equal to random
% assessor. Map it in the (0, 1] range by negative exponential so that 0  
% means different from random assessor
EXPERIMENT.command.normalize.kld = @(x, T, R) exp(-x);

% Commands to be executed to compute weights

EXPERIMENT.command.weight.sgl.msr.md = @(und, uni, ovr, T) repmat(min([und; uni; ovr]), T, 1);

EXPERIMENT.command.weight.sgl.msr.msd = @(und, uni, ovr, T) repmat(min([und.^2; uni.^2; ovr.^2]), T, 1);

EXPERIMENT.command.weight.sgl.msr.med = @(und, uni, ovr, T) repmat(sum([und; uni; ovr]), T, 1);


EXPERIMENT.command.weight.sgl.gp.md = @(und, uni, ovr, T) repmat(min([mean(und);mean(uni);mean(ovr)]), T, 1);

EXPERIMENT.command.weight.sgl.gp.msd = @(und, uni, ovr, T) repmat(min([mean(und).^2;mean(uni).^2;mean(ovr).^2]), T, 1);

EXPERIMENT.command.weight.sgl.gp.med = @(und, uni, ovr, T) repmat(sum([mean(und);mean(uni);mean(ovr)]), T, 1);


EXPERIMENT.command.weight.sgl.wgt.md = @(und, uni, ovr, T) repmat(mean(min(cat(3, und, uni, ovr), [], 3)), T, 1);

EXPERIMENT.command.weight.sgl.wgt.msd = @(und, uni, ovr, T) repmat(mean(min(cat(3, und.^2, uni.^2, ovr.^2), [], 3)), T, 1);

EXPERIMENT.command.weight.sgl.wgt.med = @(und, uni, ovr, T) repmat(mean(sum(cat(3, und, uni, ovr), 3)), T, 1);


EXPERIMENT.command.weight.tpc.msr.md = @(und, uni, ovr, T) min(cat(3, und, uni, ovr), [], 3);

EXPERIMENT.command.weight.tpc.msr.msd = @(und, uni, ovr, T) min(cat(3, und.^2, uni.^2, ovr.^2), [], 3);

EXPERIMENT.command.weight.tpc.msr.med = @(und, uni, ovr, T) sum(cat(3, und, uni, ovr), 3);


EXPERIMENT.command.weight.tpc.gp.md = @(und, uni, ovr, T) min(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), [], 3);

EXPERIMENT.command.weight.tpc.gp.msd = @(und, uni, ovr, T) min(cat(3, mean(und, 3).^2, mean(uni, 3).^2, mean(ovr, 3).^2), [], 3);

EXPERIMENT.command.weight.tpc.gp.med = @(und, uni, ovr, T) sum(cat(3, mean(und, 3), mean(uni, 3), mean(ovr, 3)), 3);


EXPERIMENT.command.weight.tpc.wgt.md = @(und, uni, ovr, T) mean(min(cat(4, und, uni, ovr), [], 4), 3);

EXPERIMENT.command.weight.tpc.wgt.msd = @(und, uni, ovr, T) mean(min(cat(4, und.^2, uni.^2, ovr.^2), [], 4), 3);

EXPERIMENT.command.weight.tpc.wgt.med = @(und, uni, ovr, T) mean(sum(cat(4, und, uni, ovr), 4), 3);


%<<<<<<<<<<

%>>>>>>>>>>>> aw_us_mono_sgl

EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.id = 'aw_us_mono_sgl_msr_fro_med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.latex.label = '\texttt{sgl\_msr\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med.matlab.label = 'sgl_msr_fro_med';

EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.id = 'aw_us_mono_sgl_msr_fro_msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.latex.label = '\texttt{sgl\_msr\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd.matlab.label = 'sgl_msr_fro_msd';

EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.id = 'aw_us_mono_sgl_msr_fro_md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.weight.command = EXPERIMENT.command.weight.sgl.msr.md;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.latex.label = '\texttt{sgl\_msr\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_md.matlab.label = 'sgl_msr_fro_md';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.id = 'aw_us_mono_sgl_msr_rmse_med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.latex.label = '\texttt{sgl\_msr\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med.matlab.label = 'sgl_msr_rmse_med';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.id = 'aw_us_mono_sgl_msr_rmse_msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.latex.label = '\texttt{sgl\_msr\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd.matlab.label = 'sgl_msr_rmse_msd';

EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.id = 'aw_us_mono_sgl_msr_rmse_md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.weight.command = EXPERIMENT.command.weight.sgl.msr.md;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.latex.label = '\texttt{sgl\_msr\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_md.matlab.label = 'sgl_msr_rmse_md';

EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.id = 'aw_us_mono_sgl_msr_kld_med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.latex.label = '\texttt{sgl\_msr\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med.matlab.label = 'sgl_msr_kld_med';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.id = 'aw_us_mono_sgl_msr_kld_msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.latex.label = '\texttt{sgl\_msr\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd.matlab.label = 'sgl_msr_kld_msd';

EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.id = 'aw_us_mono_sgl_msr_kld_md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.weight.command = EXPERIMENT.command.weight.sgl.msr.md;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.latex.label = '\texttt{sgl\_msr\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_md.matlab.label = 'sgl_msr_kld_md';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.id = 'aw_us_mono_sgl_msr_apc_med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.latex.label = '\texttt{sgl\_msr\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med.matlab.label = 'sgl_msr_apc_med';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.id = 'aw_us_mono_sgl_msr_apc_msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.latex.label = '\texttt{sgl\_msr\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd.matlab.label = 'sgl_msr_apc_msd';

EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.id = 'aw_us_mono_sgl_msr_apc_md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.weight.command = EXPERIMENT.command.weight.sgl.msr.md;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.latex.label = '\texttt{sgl\_msr\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_md.matlab.label = 'sgl_msr_apc_md';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.id = 'aw_us_mono_sgl_msr_tau_med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.latex.label = '\texttt{sgl\_msr\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med.matlab.label = 'sgl_msr_tau_med';

EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_tau_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_tau_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_tau_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_tau_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_tau_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_tau_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_tau_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_tau_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_tau_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_tau_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_tau_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_tau_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_tau_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_tau_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_tau_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_tau_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_tau_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_tau_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_tau_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_tau_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_tau_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_tau_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_tau_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_tau_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_tau_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_tau_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_tau_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_tau_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_tau_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_med_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_tau_med_rnd020_rnd095}';

EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.id = 'aw_us_mono_sgl_msr_tau_msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.latex.label = '\texttt{sgl\_msr\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd.matlab.label = 'sgl_msr_tau_msd';

EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_tau_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_tau_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_tau_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_tau_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_tau_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_tau_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_tau_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_tau_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_tau_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_tau_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_tau_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_tau_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_tau_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_tau_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_tau\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_msd_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_tau_msd_rnd020_rnd095}';

EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.id = 'aw_us_mono_sgl_msr_tau_md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.weight.command = EXPERIMENT.command.weight.sgl.msr.md;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.latex.label = '\texttt{sgl\_msr\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_tau_md.matlab.label = 'sgl_msr_tau_md';

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.id = 'aw_us_mono_sgl_gp_fro_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.oldID = 'awusmonofromed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.latex.label = '\texttt{sgl\_gp\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_med.matlab.label = 'sgl_gp_fro_med';

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.id = 'aw_us_mono_sgl_gp_fro_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.oldID = 'awusmonofromd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.latex.label = '\texttt{sgl\_gp\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_msd.matlab.label = 'sgl_gp_fro_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.id = 'aw_us_mono_sgl_gp_fro_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.latex.label = '\texttt{sgl\_gp\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_fro_md.matlab.label = 'sgl_gp_fro_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.id = 'aw_us_mono_sgl_gp_rmse_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.oldID = 'awusmonormsemed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.latex.label = '\texttt{sgl\_gp\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_med.matlab.label = 'sgl_gp_rmse_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.id = 'aw_us_mono_sgl_gp_rmse_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.oldID = 'awusmonormsemd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.latex.label = '\texttt{sgl\_gp\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_msd.matlab.label = 'sgl_gp_rmse_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.id = 'aw_us_mono_sgl_gp_rmse_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.latex.label = '\texttt{sgl\_gp\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_rmse_md.matlab.label = 'sgl_gp_rmse_md';

EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.id = 'aw_us_mono_sgl_gp_kld_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.oldID = 'awusmonokldmed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.latex.label = '\texttt{sgl\_gp\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_med.matlab.label = 'sgl_gp_kld_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.id = 'aw_us_mono_sgl_gp_kld_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.oldID = 'awusmonokldmd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.latex.label = '\texttt{sgl\_gp\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_msd.matlab.label = 'sgl_gp_kld_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.id = 'aw_us_mono_sgl_gp_kld_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.latex.label = '\texttt{sgl\_gp\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_kld_md.matlab.label = 'sgl_gp_kld_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.id = 'aw_us_mono_sgl_gp_apc_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.oldID = 'awusmonoapcmed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.latex.label = '\texttt{sgl\_gp\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_med.matlab.label = 'sgl_gp_apc_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.id = 'aw_us_mono_sgl_gp_apc_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.oldID = 'awusmonoapcmd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.latex.label = '\texttt{sgl\_gp\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_msd.matlab.label = 'sgl_gp_apc_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.id = 'aw_us_mono_sgl_gp_apc_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.latex.label = '\texttt{sgl\_gp\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_apc_md.matlab.label = 'sgl_gp_apc_md';


EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.id = 'aw_us_mono_sgl_gp_tau_med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.oldID = 'awusmonotaumed';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.weight.command = EXPERIMENT.command.weight.sgl.gp.med;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.latex.label = '\texttt{sgl\_gp\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_med.matlab.label = 'sgl_gp_tau_med';


EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.id = 'aw_us_mono_sgl_gp_tau_msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.oldID = 'awusmonotaumd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.weight.command = EXPERIMENT.command.weight.sgl.gp.msd;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.latex.label = '\texttt{sgl\_gp\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_msd.matlab.label = 'sgl_gp_tau_msd';

EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.id = 'aw_us_mono_sgl_gp_tau_md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.weight.command = EXPERIMENT.command.weight.sgl.gp.md;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.score.command = @aw_us_mono_sgl_gp;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.latex.label = '\texttt{sgl\_gp\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_gp_tau_md.matlab.label = 'sgl_gp_tau_md';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.id = 'aw_us_mono_sgl_wgt_fro_med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.weight.command = EXPERIMENT.command.weight.sgl.wgt.med;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.latex.label = '\texttt{sgl\_wgt\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_med.matlab.label = 'sgl_wgt_fro_med';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.id = 'aw_us_mono_sgl_wgt_fro_msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.weight.command = EXPERIMENT.command.weight.sgl.wgt.msd;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.latex.label = '\texttt{sgl\_wgt\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_msd.matlab.label = 'sgl_wgt_fro_msd';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.id = 'aw_us_mono_sgl_wgt_fro_md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.weight.command = EXPERIMENT.command.weight.sgl.wgt.md;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.latex.label = '\texttt{sgl\_wgt\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_fro_md.matlab.label = 'sgl_wgt_fro_md';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.id = 'aw_us_mono_sgl_wgt_rmse_med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.weight.command = EXPERIMENT.command.weight.sgl.wgt.med;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.latex.label = '\texttt{sgl\_wgt\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_med.matlab.label = 'sgl_wgt_rmse_med';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.id = 'aw_us_mono_sgl_wgt_rmse_msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.weight.command = EXPERIMENT.command.weight.sgl.wgt.msd;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.latex.label = '\texttt{sgl\_wgt\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_msd.matlab.label = 'sgl_wgt_rmse_msd';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.id = 'aw_us_mono_sgl_wgt_rmse_md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.weight.command = EXPERIMENT.command.weight.sgl.wgt.md;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.latex.label = '\texttt{sgl\_wgt\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_rmse_md.matlab.label = 'sgl_wgt_rmse_md';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.id = 'aw_us_mono_sgl_wgt_kld_med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.weight.command = EXPERIMENT.command.weight.sgl.wgt.med;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.latex.label = '\texttt{sgl\_wgt\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_med.matlab.label = 'sgl_wgt_kld_med';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.id = 'aw_us_mono_sgl_wgt_kld_msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.weight.command = EXPERIMENT.command.weight.sgl.wgt.msd;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.latex.label = '\texttt{sgl\_wgt\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_msd.matlab.label = 'sgl_wgt_kld_msd';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.id = 'aw_us_mono_sgl_wgt_kld_md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.weight.command = EXPERIMENT.command.weight.sgl.wgt.md;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.latex.label = '\texttt{sgl\_wgt\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_kld_md.matlab.label = 'sgl_wgt_kld_md';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.id = 'aw_us_mono_sgl_wgt_apc_med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.weight.command = EXPERIMENT.command.weight.sgl.wgt.med;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.latex.label = '\texttt{sgl\_wgt\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_med.matlab.label = 'sgl_wgt_apc_med';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.id = 'aw_us_mono_sgl_wgt_apc_msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.weight.command = EXPERIMENT.command.weight.sgl.wgt.msd;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.latex.label = '\texttt{sgl\_wgt\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_msd.matlab.label = 'sgl_wgt_apc_msd';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.id = 'aw_us_mono_sgl_wgt_apc_md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.weight.command = EXPERIMENT.command.weight.sgl.wgt.md;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.latex.label = '\texttt{sgl\_wgt\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_apc_md.matlab.label = 'sgl_wgt_apc_md';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.id = 'aw_us_mono_sgl_wgt_tau_med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.weight.command = EXPERIMENT.command.weight.sgl.wgt.med;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.latex.label = '\texttt{sgl\_wgt\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_med.matlab.label = 'sgl_wgt_tau_med';


EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.id = 'aw_us_mono_sgl_wgt_tau_msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.weight.command = EXPERIMENT.command.weight.sgl.wgt.msd;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.latex.label = '\texttt{sgl\_wgt\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_msd.matlab.label = 'sgl_wgt_tau_msd';

EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.id = 'aw_us_mono_sgl_wgt_tau_md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.weight.command = EXPERIMENT.command.weight.sgl.wgt.md;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.score.command = @aw_us_mono_sgl_wgt;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'sgl', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.latex.label = '\texttt{sgl\_wgt\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_wgt_tau_md.matlab.label = 'sgl_wgt_tau_md';

%<<<<<<<<<<<< aw_us_mono_sgl

%>>>>>>>>>>>> aw_us_mono_tpc

EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.id = 'aw_us_mono_tpc_msr_fro_med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.latex.label = '\texttt{tpc\_msr\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med.matlab.label = 'tpc_msr_fro_med';

EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.id = 'aw_us_mono_tpc_msr_fro_msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.latex.label = '\texttt{tpc\_msr\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd.matlab.label = 'tpc_msr_fro_msd';

EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.id = 'aw_us_mono_tpc_msr_fro_md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.weight.command = EXPERIMENT.command.weight.tpc.msr.md;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.latex.label = '\texttt{tpc\_msr\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_md.matlab.label = 'tpc_msr_fro_md';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.id = 'aw_us_mono_tpc_msr_rmse_med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.latex.label = '\texttt{tpc\_msr\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med.matlab.label = 'tpc_msr_rmse_med';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.id = 'aw_us_mono_tpc_msr_rmse_msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.latex.label = '\texttt{tpc\_msr\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd.matlab.label = 'tpc_msr_rmse_msd';

EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.id = 'aw_us_mono_tpc_msr_rmse_md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.weight.command = EXPERIMENT.command.weight.tpc.msr.md;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.latex.label = '\texttt{tpc\_msr\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_md.matlab.label = 'tpc_msr_rmse_md';

EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.id = 'aw_us_mono_tpc_msr_kld_med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.latex.label = '\texttt{tpc\_msr\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med.matlab.label = 'tpc_msr_kld_med';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.id = 'aw_us_mono_tpc_msr_kld_msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.latex.label = '\texttt{tpc\_msr\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd.matlab.label = 'tpc_msr_kld_msd';

EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.id = 'aw_us_mono_tpc_msr_kld_md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.weight.command = EXPERIMENT.command.weight.tpc.msr.md;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.latex.label = '\texttt{tpc\_msr\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_md.matlab.label = 'tpc_msr_kld_md';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.id = 'aw_us_mono_tpc_msr_apc_med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.latex.label = '\texttt{tpc\_msr\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med.matlab.label = 'tpc_msr_apc_med';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.id = 'aw_us_mono_tpc_msr_apc_msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.latex.label = '\texttt{tpc\_msr\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd.matlab.label = 'tpc_msr_apc_msd';

EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.id = 'aw_us_mono_tpc_msr_apc_md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.weight.command = EXPERIMENT.command.weight.tpc.msr.md;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.latex.label = '\texttt{tpc\_msr\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_md.matlab.label = 'tpc_msr_apc_md';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.id = 'aw_us_mono_tpc_msr_tau_med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.latex.label = '\texttt{tpc\_msr\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med.matlab.label = 'tpc_msr_tau_med';

EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_tau_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_tau_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_tau_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_tau_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_tau_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_tau_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_tau_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_tau_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_tau_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_tau_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_tau_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_tau_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_tau_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_tau_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_tau_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_tau_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_tau_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_tau_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_tau_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_tau_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_tau_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_tau_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_tau_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_tau_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_tau_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_tau_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_tau_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_tau_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_tau_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_med_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_tau_med_rnd020_rnd095}';

EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.id = 'aw_us_mono_tpc_msr_tau_msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.latex.label = '\texttt{tpc\_msr\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd.matlab.label = 'tpc_msr_tau_msd';

EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_tau_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_tau_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_tau_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_tau_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_tau_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_tau_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_tau_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_tau_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_tau_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_tau_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_tau_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_tau_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_tau_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_tau_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_tau\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_msd_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_tau_msd_rnd020_rnd095}';

EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.id = 'aw_us_mono_tpc_msr_tau_md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.weight.command = EXPERIMENT.command.weight.tpc.msr.md;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'msr');
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.latex.label = '\texttt{tpc\_msr\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_tau_md.matlab.label = 'tpc_msr_tau_md';

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.id = 'aw_us_mono_tpc_gp_fro_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.oldID = 'awusmonofromed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.latex.label = '\texttt{tpc\_gp\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_med.matlab.label = 'tpc_gp_fro_med';

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.id = 'aw_us_mono_tpc_gp_fro_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.oldID = 'awusmonofromd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.latex.label = '\texttt{tpc\_gp\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_msd.matlab.label = 'tpc_gp_fro_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.id = 'aw_us_mono_tpc_gp_fro_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.latex.label = '\texttt{tpc\_gp\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_fro_md.matlab.label = 'tpc_gp_fro_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.id = 'aw_us_mono_tpc_gp_rmse_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.oldID = 'awusmonormsemed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.latex.label = '\texttt{tpc\_gp\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_med.matlab.label = 'tpc_gp_rmse_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.id = 'aw_us_mono_tpc_gp_rmse_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.oldID = 'awusmonormsemd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.latex.label = '\texttt{tpc\_gp\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_msd.matlab.label = 'tpc_gp_rmse_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.id = 'aw_us_mono_tpc_gp_rmse_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.latex.label = '\texttt{tpc\_gp\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_rmse_md.matlab.label = 'tpc_gp_rmse_md';

EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.id = 'aw_us_mono_tpc_gp_kld_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.oldID = 'awusmonokldmed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.latex.label = '\texttt{tpc\_gp\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_med.matlab.label = 'tpc_gp_kld_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.id = 'aw_us_mono_tpc_gp_kld_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.oldID = 'awusmonokldmd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.latex.label = '\texttt{tpc\_gp\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_msd.matlab.label = 'tpc_gp_kld_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.id = 'aw_us_mono_tpc_gp_kld_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.latex.label = '\texttt{tpc\_gp\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_kld_md.matlab.label = 'tpc_gp_kld_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.id = 'aw_us_mono_tpc_gp_apc_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.oldID = 'awusmonoapcmed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.latex.label = '\texttt{tpc\_gp\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_med.matlab.label = 'tpc_gp_apc_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.id = 'aw_us_mono_tpc_gp_apc_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.oldID = 'awusmonoapcmd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.latex.label = '\texttt{tpc\_gp\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_msd.matlab.label = 'tpc_gp_apc_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.id = 'aw_us_mono_tpc_gp_apc_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.latex.label = '\texttt{tpc\_gp\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_apc_md.matlab.label = 'tpc_gp_apc_md';


EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.id = 'aw_us_mono_tpc_gp_tau_med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.oldID = 'awusmonotaumed';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.weight.command = EXPERIMENT.command.weight.tpc.gp.med;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.latex.label = '\texttt{tpc\_gp\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_med.matlab.label = 'tpc_gp_tau_med';


EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.id = 'aw_us_mono_tpc_gp_tau_msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.oldID = 'awusmonotaumd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.weight.command = EXPERIMENT.command.weight.tpc.gp.msd;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.latex.label = '\texttt{tpc\_gp\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_msd.matlab.label = 'tpc_gp_tau_msd';

EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.id = 'aw_us_mono_tpc_gp_tau_md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.aggregation.id = 'gp';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.weight.command = EXPERIMENT.command.weight.tpc.gp.md;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.score.command = @aw_us_mono_tpc_gp;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'gp');
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.latex.label = '\texttt{tpc\_gp\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_gp_tau_md.matlab.label = 'tpc_gp_tau_md';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.id = 'aw_us_mono_tpc_wgt_fro_med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.weight.command = EXPERIMENT.command.weight.tpc.wgt.med;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.latex.label = '\texttt{tpc\_wgt\_fro\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_med.matlab.label = 'tpc_wgt_fro_med';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.id = 'aw_us_mono_tpc_wgt_fro_msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.weight.command = EXPERIMENT.command.weight.tpc.wgt.msd;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.latex.label = '\texttt{tpc\_wgt\_fro\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_msd.matlab.label = 'tpc_wgt_fro_msd';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.id = 'aw_us_mono_tpc_wgt_fro_md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.weight.command = EXPERIMENT.command.weight.tpc.wgt.md;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.latex.color = 'LightSkyBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.latex.label = '\texttt{tpc\_wgt\_fro\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_fro_md.matlab.label = 'tpc_wgt_fro_md';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.id = 'aw_us_mono_tpc_wgt_rmse_med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.weight.command = EXPERIMENT.command.weight.tpc.wgt.med;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.latex.label = '\texttt{tpc\_wgt\_rmse\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_med.matlab.label = 'tpc_wgt_rmse_med';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.id = 'aw_us_mono_tpc_wgt_rmse_msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.weight.command = EXPERIMENT.command.weight.tpc.wgt.msd;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.latex.label = '\texttt{tpc\_wgt\_rmse\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_msd.matlab.label = 'tpc_wgt_rmse_msd';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.id = 'aw_us_mono_tpc_wgt_rmse_md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.weight.command = EXPERIMENT.command.weight.tpc.wgt.md;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.latex.color = 'SteelBlue1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.latex.label = '\texttt{tpc\_wgt\_rmse\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_rmse_md.matlab.label = 'tpc_wgt_rmse_md';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.id = 'aw_us_mono_tpc_wgt_kld_med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.weight.command = EXPERIMENT.command.weight.tpc.wgt.med;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.latex.label = '\texttt{tpc\_wgt\_kld\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_med.matlab.label = 'tpc_wgt_kld_med';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.id = 'aw_us_mono_tpc_wgt_kld_msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.weight.command = EXPERIMENT.command.weight.tpc.wgt.msd;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.latex.label = '\texttt{tpc\_wgt\_kld\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_msd.matlab.label = 'tpc_wgt_kld_msd';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.id = 'aw_us_mono_tpc_wgt_kld_md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.weight.command = EXPERIMENT.command.weight.tpc.wgt.md;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.latex.color = 'Bisque1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.latex.label = '\texttt{tpc\_wgt\_kld\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_kld_md.matlab.label = 'tpc_wgt_kld_md';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.id = 'aw_us_mono_tpc_wgt_apc_med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.weight.command = EXPERIMENT.command.weight.tpc.wgt.med;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.latex.label = '\texttt{tpc\_wgt\_apc\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_med.matlab.label = 'tpc_wgt_apc_med';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.id = 'aw_us_mono_tpc_wgt_apc_msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.weight.command = EXPERIMENT.command.weight.tpc.wgt.msd;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.latex.label = '\texttt{tpc\_wgt\_apc\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_msd.matlab.label = 'tpc_wgt_apc_msd';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.id = 'aw_us_mono_tpc_wgt_apc_md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.weight.command = EXPERIMENT.command.weight.tpc.wgt.md;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.latex.color = 'DarkSeaGreen1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.latex.label = '\texttt{tpc\_wgt\_apc\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_apc_md.matlab.label = 'tpc_wgt_apc_md';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.id = 'aw_us_mono_tpc_wgt_tau_med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.weight.command = EXPERIMENT.command.weight.tpc.wgt.med;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.latex.label = '\texttt{tpc\_wgt\_tau\_med}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_med.matlab.label = 'tpc_wgt_tau_med';


EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.id = 'aw_us_mono_tpc_wgt_tau_msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.weight.command = EXPERIMENT.command.weight.tpc.wgt.msd;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.latex.label = '\texttt{tpc\_wgt\_tau\_msd}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_msd.matlab.label = 'tpc_wgt_tau_msd';

EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.id = 'aw_us_mono_tpc_wgt_tau_md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.aggregation.id = 'wgt';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.gap.id = 'tau';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.gap.normalization = EXPERIMENT.command.normalize.tau;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.weight.id = 'md';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.weight.command = EXPERIMENT.command.weight.tpc.wgt.md;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.score.command = @aw_us_mono_tpc_wgt;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.relativePath = sprintf('%1$s%2$s%3$s%2$s', 'tpc', filesep, 'wgt');
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.latex.label = '\texttt{tpc\_wgt\_tau\_md}';
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_wgt_tau_md.matlab.label = 'tpc_wgt_tau_md';

%<<<<<<<<<<<< aw_us_mono_tpc

EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_fro_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_fro_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_fro_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_fro_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_fro_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_fro_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_fro_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_fro_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_fro_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_fro_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_fro_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_fro_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_fro_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_fro_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_fro_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_fro_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_fro_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_fro_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_fro_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_fro_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_fro_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_fro_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_fro_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_fro_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_fro_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_fro_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_fro_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_fro_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_fro_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_med_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_fro_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_fro_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_fro_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_fro_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_fro_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_fro_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_fro_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_fro_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_fro_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_fro_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_fro_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_fro_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_fro_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_fro_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_fro_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_fro\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_fro_msd_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_fro_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_rmse_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_rmse_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_rmse_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_rmse_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_rmse_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_rmse_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_rmse_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_rmse_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_rmse_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_rmse_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_rmse_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_rmse_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_rmse_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_rmse_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_med_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_rmse_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_rmse\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_rmse_msd_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_rmse_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_kld_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_kld_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_kld_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_kld_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_kld_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_kld_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_kld_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_kld_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_kld_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_kld_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_kld_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_kld_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_kld_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_kld_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_kld_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_kld_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_kld_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_kld_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_kld_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_kld_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_kld_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_kld_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_kld_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_kld_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_kld_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_kld_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_kld_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_kld_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_kld_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_med_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_kld_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_kld_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_kld_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_kld_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_kld_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_kld_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_kld_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_kld_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_kld_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_kld_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_kld_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_kld_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_kld_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_kld_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_kld_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_kld\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_kld_msd_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_kld_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_apc_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_apc_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_apc_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_apc_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_apc_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_apc_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_apc_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_apc_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_apc_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_apc_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_apc_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_apc_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_apc_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_apc_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_apc_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_apc_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_apc_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_apc_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_apc_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_apc_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_apc_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_apc_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_apc_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_apc_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_apc_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_apc_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_apc_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_apc_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_apc_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.med;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_med_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_apc_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.id = 'aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd080.matlab.label = '\texttt{sgl_msr_apc_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.id = 'aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd085.matlab.label = '\texttt{sgl_msr_apc_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.id = 'aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd005_rnd090.matlab.label = '\texttt{sgl_msr_apc_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.id = 'aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd080.matlab.label = '\texttt{sgl_msr_apc_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.id = 'aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd085.matlab.label = '\texttt{sgl_msr_apc_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.id = 'aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd090.matlab.label = '\texttt{sgl_msr_apc_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.id = 'aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd010_rnd095.matlab.label = '\texttt{sgl_msr_apc_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.id = 'aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd080.matlab.label = '\texttt{sgl_msr_apc_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.id = 'aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd085.matlab.label = '\texttt{sgl_msr_apc_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.id = 'aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd090.matlab.label = '\texttt{sgl_msr_apc_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.id = 'aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd015_rnd095.matlab.label = '\texttt{sgl_msr_apc_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.id = 'aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd080.matlab.label = '\texttt{sgl_msr_apc_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.id = 'aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd085.matlab.label = '\texttt{sgl_msr_apc_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.id = 'aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd090.matlab.label = '\texttt{sgl_msr_apc_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.id = 'aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.granularity.id = 'sgl';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.sgl.msr.msd;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.score.command = @aw_us_mono_sgl_msr;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.relativePath = 'sgl/msr/';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.latex.label = '\texttt{sgl\_msr\_apc\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_sgl_msr_apc_msd_rnd020_rnd095.matlab.label = '\texttt{sgl_msr_apc_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_fro_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_fro_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_fro_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_fro_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_fro_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_fro_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_fro_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_fro_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_fro_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_fro_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_fro_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_fro_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_fro_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_fro_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_fro_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_fro_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_fro_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_fro_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_fro_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_fro_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_fro_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_fro_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_fro_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_fro_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_fro_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_fro_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_fro_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_fro_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_fro_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_med_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_fro_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_fro_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_fro_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_fro_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_fro_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_fro_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_fro_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_fro_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_fro_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_fro_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_fro_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_fro_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_fro_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_fro_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_fro_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.gap.id = 'fro';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.fro;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_fro\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_fro_msd_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_fro_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_rmse_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_rmse_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_rmse_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_rmse_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_rmse_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_rmse_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_rmse_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_rmse_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_rmse_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_rmse_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_rmse_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_rmse_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_rmse_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_rmse_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_med_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_rmse_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.gap.id = 'rmse';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.rmse;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_rmse\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_rmse_msd_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_rmse_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_kld_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_kld_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_kld_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_kld_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_kld_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_kld_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_kld_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_kld_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_kld_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_kld_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_kld_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_kld_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_kld_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_kld_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_kld_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_kld_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_kld_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_kld_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_kld_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_kld_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_kld_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_kld_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_kld_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_kld_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_kld_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_kld_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_kld_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_kld_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_kld_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_med_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_kld_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_kld_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_kld_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_kld_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_kld_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_kld_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_kld_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_kld_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_kld_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_kld_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_kld_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_kld_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_kld_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_kld_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_kld_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.gap.id = 'kld';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.kld;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_kld\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_kld_msd_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_kld_msd_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_apc_med_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_apc_med_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_apc_med_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_apc_med_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_apc_med_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_apc_med_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_apc_med_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_apc_med_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_apc_med_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_apc_med_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_apc_med_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_apc_med_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_apc_med_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_apc_med_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_apc_med_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_apc_med_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_apc_med_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_apc_med_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_apc_med_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_apc_med_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_apc_med_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_apc_med_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_apc_med_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_apc_med_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_apc_med_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_apc_med_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_apc_med_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_apc_med_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_apc_med_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.weight.id = 'med';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.med;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_med\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_med_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_apc_med_rnd020_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.id = 'aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd005\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd080.matlab.label = '\texttt{tpc_msr_apc_msd_rnd005_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.id = 'aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd005\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd085.matlab.label = '\texttt{tpc_msr_apc_msd_rnd005_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.id = 'aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.und = EXPERIMENT.tag.rnd005.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd005\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd005_rnd090.matlab.label = '\texttt{tpc_msr_apc_msd_rnd005_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.id = 'aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd010\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd080.matlab.label = '\texttt{tpc_msr_apc_msd_rnd010_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.id = 'aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd010\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd085.matlab.label = '\texttt{tpc_msr_apc_msd_rnd010_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.id = 'aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd010\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd090.matlab.label = '\texttt{tpc_msr_apc_msd_rnd010_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.id = 'aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.und = EXPERIMENT.tag.rnd010.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd010\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd010_rnd095.matlab.label = '\texttt{tpc_msr_apc_msd_rnd010_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.id = 'aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd015\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd080.matlab.label = '\texttt{tpc_msr_apc_msd_rnd015_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.id = 'aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd015\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd085.matlab.label = '\texttt{tpc_msr_apc_msd_rnd015_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.id = 'aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd015\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd090.matlab.label = '\texttt{tpc_msr_apc_msd_rnd015_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.id = 'aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.und = EXPERIMENT.tag.rnd015.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd015\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd015_rnd095.matlab.label = '\texttt{tpc_msr_apc_msd_rnd015_rnd095}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.id = 'aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.ovr = EXPERIMENT.tag.rnd080.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd020\_rnd080}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd080.matlab.label = '\texttt{tpc_msr_apc_msd_rnd020_rnd080}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.id = 'aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.ovr = EXPERIMENT.tag.rnd085.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd020\_rnd085}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd085.matlab.label = '\texttt{tpc_msr_apc_msd_rnd020_rnd085}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.id = 'aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.ovr = EXPERIMENT.tag.rnd090.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd020\_rnd090}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd090.matlab.label = '\texttt{tpc_msr_apc_msd_rnd020_rnd090}';


EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.id = 'aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.family.id = 'aw';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.learning.id = 'us';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.feature.id = 'mono';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.granularity.id = 'tpc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.aggregation.id = 'msr';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.gap.id = 'apc';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.gap.normalization = EXPERIMENT.command.normalize.apc;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.weight.id = 'msd';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.weight.command = EXPERIMENT.command.weight.tpc.msr.msd;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.score.command = @aw_us_mono_tpc_msr;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.score.callerFunction = 'compute_aware_unsupervised_mono_measures';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.relativePath = 'tpc/msr/';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.und = EXPERIMENT.tag.rnd020.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.uni = EXPERIMENT.tag.rnd050.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.ovr = EXPERIMENT.tag.rnd095.id;
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.latex.color = 'OliveDrab1';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.latex.label = '\texttt{tpc\_msr\_apc\_msd\_rnd020\_rnd095}';
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.matlab.color = [.69,.888,1];
EXPERIMENT.tag.aw_us_mono_tpc_msr_apc_msd_rnd020_rnd095.matlab.label = '\texttt{tpc_msr_apc_msd_rnd020_rnd095}';















%% Patterns for LOG files

% Pattern EXPERIMENT.path.base/log/import_collection_<trackID>.log
EXPERIMENT.pattern.logFile.importCollection = @(trackID) sprintf('%1$simport_collection_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<tag>_measures_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasures = @(trackID, tag) sprintf('%1$scompute_%2$s_measures_%3$s.log', EXPERIMENT.path.log, tag, trackID);

% Pattern EXPERIMENT.path.base/log/generate_kuples_<trackID>.log
EXPERIMENT.pattern.logFile.generateKuples = @(trackID) sprintf('%1$sgenerate_kuples_%2$s.log', EXPERIMENT.path.log, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_poold_<trackID>.log
EXPERIMENT.pattern.logFile.computePools = @(experimentTag, trackID) sprintf('%1$scompute_%2$s_pools_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_pool_measures_slice-<startKupleSet>-<endKupleSet>-<startKuple>-<endKuple>_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasures = @(experimentTag, startKupleSet, endKupleSet, startKuple, endKuple, trackID) ...
    sprintf('%1$scompute_%2$s_pool_measures_slice-%3$d-%4$d-%5$d-%6$d_%7$s.log', EXPERIMENT.path.log, experimentTag, ...
            startKupleSet, endKupleSet, startKuple, endKuple, trackID);

% Pattern EXPERIMENT.path.base/log/compute_<gap>_<experimentTag>_pool_measures_gap_<trackID>.log
EXPERIMENT.pattern.logFile.computePoolMeasuresGap = @(gap, experimentTag, trackID) ...
    sprintf('%1$scompute_%2$s_%3$s_pool_measures_gap_%4$s.log', EXPERIMENT.path.log, gap, experimentTag, trackID);
        
% Pattern EXPERIMENT.path.base/log/compute_<experimentTag>_measures_pdf_<trackID>.log
EXPERIMENT.pattern.logFile.computeMeasuresPdf = @(experimentTag, trackID) ...
    sprintf('%1$scompute_%2$s_measures_pdf_%3$s.log', EXPERIMENT.path.log, experimentTag, trackID);

% Pattern EXPERIMENT.path.base/log/analyse_<tag>_measures_slice-<startKupleSet>-<endKupleSet>_<trackID>.log
EXPERIMENT.pattern.logFile.analyseMeasures = @(trackID, tag, startKupleSet, endKupleSet) sprintf('%1$sanalyse_%2$s_measures_slice-%3$d-%4$d_%5$s.log', EXPERIMENT.path.log, tag, startKupleSet, endKupleSet, trackID);

% Pattern EXPERIMENT.path.base/log/print_report_<desc>_<trackID>.log
EXPERIMENT.pattern.logFile.printReport = @(trackID, desc) sprintf('%1$sprint_report_%2$s_%3$s.log', EXPERIMENT.path.log, desc, trackID);


%% Patterns for MAT files

% Pattern EXPERIMENT.path.base/dataset/<trackID>/EXPERIMENT.tag.base.id/dataset_<trackShortID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$sdataset_%5$s.mat', EXPERIMENT.path.base, filesep, trackID, EXPERIMENT.tag.base.id, trackShortID);

% Pattern EXPERIMENT.path.base/dataset/<trackID>/kuples_<trackShortID>.mat
EXPERIMENT.pattern.file.kuples = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$skuples_%4$s.mat', EXPERIMENT.path.base, filesep, trackID, trackShortID);

% Pattern EXPERIMENT.path.base/dataset/<trackID>/<experimentTag>/<poolID>
EXPERIMENT.pattern.file.pool = @(trackID, experimentTag, poolID) ...
    sprintf('%1$sdataset%2$s%3$s%2$s%4$s%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, poolID);

% Pattern EXPERIMENT.path.base/measure/<trackID>/<relativePath>/<experimentTag>/<measureID>
EXPERIMENT.pattern.file.measure = @(trackID, relativePath, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID);


EXPERIMENT.pattern.file.measureK15 = @(trackID, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s.k15%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, measureID);

EXPERIMENT.pattern.file.measureK29 = @(trackID, experimentTag, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s.k29%2$s%5$s.mat', EXPERIMENT.path.base, filesep, trackID, experimentTag, measureID);

% Pattern EXPERIMENT.path.base/analysis/<trackID>/<relativePath>/<experimentTag>/<measureID>
EXPERIMENT.pattern.file.analysis = @(trackID, relativePath, experimentTag, measureID) ...
    sprintf('%1$sanalysis%2$s%3$s%2$s%4$s%5$s%2$s%6$s.mat', EXPERIMENT.path.base, filesep, trackID, relativePath, experimentTag, measureID);

% Pattern EXPERIMENT.path.base/pre-compute/<granularity>/<aggregation>/<pm>/<trackID>/<experimentTag>/<preID>
EXPERIMENT.pattern.file.pre = @(granularity, aggregation, pm, trackID, experimentTag, preID) ...
    sprintf('%1$spre-compute%2$s%3$s%2$s%4$s%2$s%5$s%2$s%6$s%2$s%7$s%2$s%8$s.mat', ...
    EXPERIMENT.path.base, filesep, granularity, aggregation, pm, trackID, experimentTag, preID);

% Pattern EXPERIMENT.path.base/report/<trackID>/<type>_report_<trackID>.mat
EXPERIMENT.pattern.file.report = @(type, trackID) sprintf('%1$sreport%2$s%4$s%2$s%3$s_report_%4$s.tex', EXPERIMENT.path.base, filesep, type, trackID);

% Pattern EXPERIMENT.path.base/analysis/<trackID>/anova/anova_<trackID>.mat
EXPERIMENT.pattern.file.anova = @(trackID) sprintf('%1$sanalysis%2$sanova%2$sanova_%3$s.mat', EXPERIMENT.path.base, filesep, trackID);

% Pattern EXPERIMENT.path.base/figure/<trackID>/<relativePath>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, relativePath, figureID) sprintf('%1$sfigure%2$s%3$s%2$s%4$s%2$s%5$s.pdf', EXPERIMENT.path.base, filesep, trackID, relativePath, figureID);


%% Patterns for identifiers

% Pattern pool_<experimentTag>_<poolLabel>_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(experimentTag, poolLabel, trackID) sprintf('pool_%1$s_%2$s_%3$s', experimentTag, poolLabel, trackID);

% Pattern pool_base_GOLD_<trackID>
EXPERIMENT.pattern.identifier.goldPool = @(trackID) EXPERIMENT.pattern.identifier.pool(EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, trackID);

% Pattern runSet_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.runSet = @(trackID, originalTrackID) sprintf('runSet_p%1$sr%2$s', trackID, originalTrackID);

% Pattern k<size>
EXPERIMENT.pattern.identifier.kuple = @(k) sprintf('k%1$02d', EXPERIMENT.kuples.sizes(k));
EXPERIMENT.pattern.identifier.oldKuple = @(k) sprintf('k%1$02d', k);

% Pattern kuples_k<size>_<trackID>
EXPERIMENT.pattern.identifier.kuples = @(k, trackID) sprintf('kuples_k%1$02d_%2$s', k, trackID);

% Pattern k<size>_s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledKuple = @(k, s) sprintf('k%1$02d_s%2$04d', k, s);

% Pattern s<sampleNumber>
EXPERIMENT.pattern.identifier.sampledRndPool = @(s) sprintf('s%1$04d', s);

% Pattern <measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.measure = @(measureID, experimentTag, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s_%3$s_p%4$sr%5$s', lower(measureID), experimentTag, experimentLabel, trackID, originalTrackID);

% Pattern <pm>_<measureID>_<experimentTag>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.pm = @(pm, measureID, experimentTag, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s', pm, EXPERIMENT.pattern.identifier.measure(measureID, experimentTag, experimentLabel, trackID, originalTrackID));


% Pattern accuracy_<experimentTag>_<experimentLabel>_<trackID>
EXPERIMENT.pattern.identifier.accuracy =  @(experimentTag, experimentLabel, trackID) ...
    sprintf('accuracy_%1$s_%2$s_%3$s', experimentTag, experimentLabel, trackID);


% Pattern multcompare_<metric>_<measure>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.multcompare.figure = @(metric, measure, shortTrackID, originalShortTrackID) ...
    sprintf('multcompare_%1$s_%2$s_p%3$sr%4$s', lower(metric), lower(measure), shortTrackID, originalShortTrackID);

% Pattern anovaTable_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.table = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaTable_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern anovaStats_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.stats = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('anovaStats_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);

% Pattern soa_<metric>_<kuple>_<shortTrackID>_<originalShortTrackID>
EXPERIMENT.pattern.identifier.anova.soa = @(measureID, kuple, shortTrackID, originalShortTrackID) ...
    sprintf('soa_%1$s_%2$s_p%3$sr%4$s', lower(measureID), kuple, shortTrackID, originalShortTrackID);


%% Configuration for collection TREC 21, 2012, Crowdsourcing

EXPERIMENT.TREC_21_2012_Crowd.id = 'TREC_21_2012_Crowd';
EXPERIMENT.TREC_21_2012_Crowd.shortID = 'T21';
EXPERIMENT.TREC_21_2012_Crowd.name =  'TREC 21, 2012, Crowdsourcing';

EXPERIMENT.TREC_21_2012_Crowd.collection.path.base = '/Users/ferro/Documents/experimental-collections/TREC/TREC_21_2012_Crowd/';
EXPERIMENT.TREC_21_2012_Crowd.collection.path.crowd = sprintf('%1$s%2$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'crowd', filesep);
EXPERIMENT.TREC_21_2012_Crowd.collection.path.runSet = @(originalTrackID) sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'runs', filesep, originalTrackID);

EXPERIMENT.TREC_21_2012_Crowd.collection.file.goldPool = sprintf('%1$s%2$s', EXPERIMENT.TREC_21_2012_Crowd.collection.path.base, 'trat-adjudicated-qrels.txt');

EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id = {'TREC_08_1999_AdHoc', 'TREC_13_2004_Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.shortID = {'T08', 'T13'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.name = {'TREC 08, 1999, AdHoc', 'TREC 13, 2004, Robust'};
EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.number = length(EXPERIMENT.TREC_21_2012_Crowd.collection.runSet.originalTrackID.id);


%% Commands to be executed

% Utility commands
EXPERIMENT.command.util.varList2Cell = @(varargin)(deal(varargin));
EXPERIMENT.command.util.assignVars =  @(varargin)(varargin{:});

% Commands to be executed to import TREC_21_2012_Crowd
EXPERIMENT.command.TREC_21_2012_Crowd.importGoldPool = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'Delimiter', 'space',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importCrowdPool = @(fileName, id, requiredTopics) importCrowd2012Pool('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'RequiredTopics', requiredTopics, 'Delimiter', 'tab',  'Verbose', false);
EXPERIMENT.command.TREC_21_2012_Crowd.importRunSet = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', 'TrecEvalLexDesc', 'SinglePrecision', true, 'Verbose', false);

% Commands to be executed to compute the measures
EXPERIMENT.command.measure.ap = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix);
EXPERIMENT.command.measure.p10 = @(pool, runSet, shortNameSuffix) precision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Cutoffs', 10);
EXPERIMENT.command.measure.rbp = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', 0.80);
EXPERIMENT.command.measure.ndcg20 = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', 20, 'Normalize', true, 'FixNumberRetrievedDocuments', 1000, 'FixedNumberRetrievedDocumentsPaddingStrategy', 'NotRelevant');
EXPERIMENT.command.measure.err20 =  @(pool, runSet, shortNameSuffix) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', 20, 'FixNumberRetrievedDocuments', 1000, 'FixedNumberRetrievedDocumentsPaddingStrategy', 'NotRelevant');

% Commands to be executed to generate kuples
EXPERIMENT.command.kuples.generate = @(poolsIdx, kupleSize) combnk(poolsIdx, kupleSize);
EXPERIMENT.command.kuples.sample = @(kuples, K, samples) kuples(randperm(K, samples), :);

% Commands to be executed to compute MV pools
EXPERIMENT.command.mvPools.compute = @(poolID, varargin) majorityVotePool(poolID, varargin{:});

% Commands to be executed to compute EM pools
EXPERIMENT.command.emPools.compute = @(tag, poolID, varargin) EXPERIMENT.tag.(tag).em(EXPERIMENT.em.tolerance, EXPERIMENT.em.maxIterations, poolID, varargin{:});

% Commands to be executed to compute RND pools
EXPERIMENT.command.rndPools.compute = @(tag, poolID, goldPool) randomPool(poolID, goldPool, EXPERIMENT.tag.(tag).ratio);
        
% Commands to be executed to compute pool measures
EXPERIMENT.command.computePoolMeasures.kupleSize = @(kuples) size(kuples, 1);

% Commands to be executed to compute gaps from random pools
EXPERIMENT.command.rndPools.gap.tpc.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.tpc.apc = @(random, assessor) apCorr(random(:), assessor(:), 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.tpc.tau = @(random, assessor) corr(random(:), assessor(:), 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.tpc.rmse = @(random, assessor) rmseCoeff(random(:), assessor(:));
EXPERIMENT.command.rndPools.gap.tpc.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.tpc.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});


EXPERIMENT.command.rndPools.gap.sgl.fro = @(random, assessor) norm(random - assessor, 'fro');
EXPERIMENT.command.rndPools.gap.sgl.apc = @(random, assessor) apCorr(nanmean(random).', nanmean(assessor).', 'Ties', true, 'TiesSamples', EXPERIMENT.analysis.apcorrTiesSamples);
EXPERIMENT.command.rndPools.gap.sgl.tau = @(random, assessor) corr(nanmean(random).', nanmean(assessor).', 'type', 'Kendall');
EXPERIMENT.command.rndPools.gap.sgl.rmse = @(random, assessor) rmseCoeff(nanmean(random).', nanmean(assessor).');
EXPERIMENT.command.rndPools.gap.sgl.kde = @(measure) kdEstimation(measure(:), EXPERIMENT.analysis.kdeBins, EXPERIMENT.analysis.kdeBandwidth);
EXPERIMENT.command.rndPools.gap.sgl.kld = @(q, p) klDivergence(EXPERIMENT.analysis.kdeBins, q, p{:});

% Commands to be executed to compute AWARE measures
EXPERIMENT.command.aware = @(a, kuples, measures) aware(a, kuples, measures{:});

% Commands to be executed to analyse AWARE measures
EXPERIMENT.command.analyse = @(goldMeasure, awareMeasure) analyseKuples(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure);


