%% aware
% 
% Generates evaluation measures driven by assessors' weights.

%% Synopsis
%
%   [awareMeasures] = aware(a, k, varargin)
%  
%
% *Parameters*
%
% * *|a|* - a normalized vector of assessors' weights. It must have as many
% elements as the number of combined measures.
% * *|kuples|* - an integer valued matrix, representing the k-uples to be used
% themselves, i.e. the indexes of the assessors to be combined
% * *|varargin|* - the measures to be combined.
%
%
% *Returns*
%
% * *|awareMeasures|*  - a matrix containing the weighted measures. 
% Each TxR plane is a  weighted kuple for T topics and R runs and the 
% K planes correspond to each possible kuple.
%

%% References
% 
% Please refer to:
%
% * Ferrante, M., Ferro, N., and Maistro, M. (2015). UNIPD Internal Report.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [awareMeasures] = aware(a, kuples, varargin)

    % check that a is a nonempty "real" matrix greater than 0 
    % and less than or equal to 1 with k elements
    validateattributes(a, {'numeric'}, ...
        {'nonempty', 'ndims', 2, 'real'}, '', 'a');
        
    % the number of measures to be combined
    M = length(varargin);
    
    assert(M == size(a, 2), 'The number of columns in the assessor weights must be equal to the number of measures to be combined');
                   
    % check that the number of element kuples is a nonempty
    % integer matrix with values greater than 0 and less than M
    validateattributes(kuples, {'numeric'}, ...
        {'nonempty', 'integer', '>', 0, '<=', M, 'ndims', 2}, '', 'kuples');
    
    assert(size(kuples, 2) <= M, 'The number of elements in a kuple must be less than or equal to the number of measures/assessors to be combined.');

    % The total number of kuples to be processed
    K = size(kuples, 1);
    
    % The actual number of assessor to be combined
    A = size(kuples, 2);
    
    % total number of topics
    T = height(varargin{1});
    
    assert(T == size(a, 1), 'The number of rows in the assessor weights must be equal to the number of topics for each measure');

    % total number of runs
    R = width(varargin{1});

    % the raw data, each TxR plane is a measure for T topics and R runs  
    % to be weighted by assessors' scores and the M planes correspond
    % to the different assessors
    data = NaN(T, R, M);

    % for each measure fill in the data cube
    for i = 1:M
        assert(height(varargin{i}) == T, 'All the measures must have the same number of topics');
        assert(width(varargin{i}) == R, 'All the measures must have the same number of runs');

        % extract the data from the k-th measure
        data(:, :, i) = varargin{i}{:, :};
    end;
    
    % generate the weights for the assessors 
    % 1) move the a plane to the third dimension with reshape(a, T, 1, M)
    % 2) repeat the moved a plane for each run with repmat(*, 1, R, 1) 
    w = repmat(reshape(a, T, 1, M), 1, R, 1);
        
    % the weighted measures, each TxR plane is a weighted kuple for T topics 
    % and R runs and the K planes correspond to each possible kuple
    awareMeasures = NaN(T, R, K);
        
    % for each k-uple
    for k = 1:K

        % the current k-uple, i.e. the k-th subplane
        currentKuple = data(:, :, kuples(k, :));

        % The weights w are taken in the same order as the kuple so
        % that each assessor always receives the same relative weight,
        % whatever his/her position in the kuple
        currentW = w(:, :, kuples(k, :));
        

        % Normalize the weight along 3rd dimension so that they sum up
        % to 1
        % 1) compute the sum along the third dimension sum(currentW, 3)
        % 2) replicate it by the number of actually combined assessors in
        % the third dimension repmat(*, 1, 1, A)
        % 3) divide
        currentW = currentW ./ repmat(sum(currentW, 3), 1, 1, A);

        % create an weighted aggregated measure across the planes
        currentKuple = dot(currentKuple, currentW, 3);
        
        % save the current kuple for output
        awareMeasures(:, :, k) = currentKuple;

    end; % for each kuple


end
