

granularity = {'sgl', 'tpc'};
aggregation = {'msr'};
gap = {'fro', 'rmse', 'kld', 'apc'};
%gap = {'tau'};
weight = {'med', 'msd'};
left_rnd = {'rnd005', 'rnd010', 'rnd015', 'rnd020',};
mid_rnd = 'rnd050';
right_rnd = {'rnd080', 'rnd085', 'rnd090', 'rnd095'};

name = @(granularity, aggregation, gap, weight, left_rnd, right_rnd) ...
    sprintf('aw_us_mono_%s_%s_%s_%s_%s_%s', granularity, aggregation, gap, weight, left_rnd, right_rnd);

for grt = 1:length(granularity)
    for agg = 1:length(aggregation)
        for gp = 1:length(gap)
            for w = 1:length(weight)
                for lrp = 1:length(left_rnd)
                    for rrp = 1:length(right_rnd)
                        
                        % avoid the already considered cases
                        if strcmp(left_rnd{lrp}, 'rnd005') && strcmp(right_rnd{rrp}, 'rnd095')
                            continue;
                        end;
                        
                        tag = name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp});
                        
                        compute_aware_unsupervised_mono_measures('TREC_21_2012_Crowd', tag);
                        
                    end;
                end;
            end;
        end;
    end;
end;
