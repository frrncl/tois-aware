
granularity = {'sgl', 'tpc'};
aggregation = {'msr'};
gap = {'fro', 'rmse', 'kld', 'apc'};
%gap = {'tau'};
weight = {'med', 'msd'};
left_rnd = {'rnd005', 'rnd010', 'rnd015', 'rnd020',};
mid_rnd = 'rnd050';
right_rnd = {'rnd080', 'rnd085', 'rnd090', 'rnd095'};

name = @(granularity, aggregation, gap, weight, left_rnd, right_rnd) ...
    sprintf('aw_us_mono_%s_%s_%s_%s_%s_%s', granularity, aggregation, gap, weight, left_rnd, right_rnd);


for grt = 1:length(granularity)
    for agg = 1:length(aggregation)
        for gp = 1:length(gap)
            for w = 1:length(weight)
                for lrp = 1:length(left_rnd)
                    for rrp = 1:length(right_rnd)
                        
                        % avoid the already considered cases
                        if strcmp(left_rnd{lrp}, 'rnd005') && strcmp(right_rnd{rrp}, 'rnd095')
                            continue;
                        end;
                        
                        % mkdir(sprintf('/Users/ferro/Documents/pubblicazioni/2015/IPM2015-FFM/experiment/measure/TREC_21_2012_Crowd/%s/msr/%s', ...
                        %  granularity{grt}, name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp})));
                        
                        % mkdir(sprintf('/Users/ferro/Documents/pubblicazioni/2015/IPM2015-FFM/experiment/analysis2/TREC_21_2012_Crowd/%s/msr/%s', ...
                        %  granularity{grt}, name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp})));
                        
                        
                        tag = name(granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp});
%                         
%                         fprintf('\n');
%                         fprintf('EXPERIMENT.tag.%1$s.id = ''%1$s'';\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.family.id = ''aw'';\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.learning.id = ''us'';\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.feature.id = ''mono'';\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.granularity.id = ''%2$s'';\n', tag, granularity{grt});
%                         fprintf('EXPERIMENT.tag.%1$s.aggregation.id = ''%2$s'';\n', tag, aggregation{agg});
%                         fprintf('EXPERIMENT.tag.%1$s.gap.id = ''%2$s'';\n', tag, gap{gp});
%                         fprintf('EXPERIMENT.tag.%1$s.gap.normalization = EXPERIMENT.command.normalize.%2$s;\n', tag, gap{gp});
%                         fprintf('EXPERIMENT.tag.%1$s.weight.id = ''%2$s'';\n', tag, weight{w});
%                         fprintf('EXPERIMENT.tag.%1$s.weight.command = EXPERIMENT.command.weight.%2$s.%3$s.%4$s;\n', tag, granularity{grt}, aggregation{agg}, weight{w});
%                         fprintf('EXPERIMENT.tag.%1$s.score.command = @aw_us_mono_%2$s_%3$s;\n', tag, granularity{grt}, aggregation{agg});
%                         fprintf('EXPERIMENT.tag.%1$s.score.callerFunction = ''compute_aware_unsupervised_mono_measures'';\n', tag, granularity{grt}, aggregation{agg});
%                         fprintf('EXPERIMENT.tag.%1$s.relativePath = ''%2$s%3$s%4$s%3$s'';\n', tag, granularity{grt}, filesep, aggregation{agg});
%                         fprintf('EXPERIMENT.tag.%1$s.und = EXPERIMENT.tag.%2$s.id;\n', tag, left_rnd{lrp});
%                         fprintf('EXPERIMENT.tag.%1$s.uni = EXPERIMENT.tag.%2$s.id;\n', tag, mid_rnd);
%                         fprintf('EXPERIMENT.tag.%1$s.ovr = EXPERIMENT.tag.%2$s.id;\n', tag, right_rnd{rrp});
%                         fprintf('EXPERIMENT.tag.%1$s.latex.color = ''OliveDrab1'';\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.latex.label = ''\\texttt{%2$s\\_%3$s\\_%4$s\\_%5$s\\_%6$s\\_%7$s}'';\n', tag, granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp});
%                         fprintf('EXPERIMENT.tag.%1$s.matlab.color = [.69,.888,1];\n', tag);
%                         fprintf('EXPERIMENT.tag.%1$s.matlab.label = ''\\texttt{%2$s_%3$s_%4$s_%5$s_%6$s_%7$s}'';\n', tag, granularity{grt}, aggregation{agg}, gap{gp}, weight{w}, left_rnd{lrp}, right_rnd{rrp});
%                         fprintf('\n');

                         fprintf('\n');
                         fprintf('XXXX \n');
                         fprintf('\t ./analyse_aware_unsupervised_measures.sh TREC_21_2012_Crowd %s 1 29\n', tag);
                         fprintf('\n');
                                                
                    end;
                end;
            end;
        end;
    end;
end;
