%% anova
% 
% Print the detailed analyses and saves them tex file.
%
%% Synopsis
%
%   [] = anova(trackID, color, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|varargin|* - the identifiers of the experiments to print.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = anovas(trackID)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;
    
    granularity.list = {'sgl', 'tpc'};
    granularity.number = length(granularity.list);
    
    aggregation.list = {'msr', 'gp', 'wgt'};
    aggregation.number = length(aggregation.list);
    
    gap.list = {'fro', 'rmse', 'kld', 'apc', 'tau'};
    gap.number = length(gap.list);
    
    weight.list = {'md', 'msd', 'med'};
    weight.number = length(weight.list);
            
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('anova', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('anova', trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing ANOVA on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    fprintf('+ Loading analyses\n');
    
        
        
     EXPERIMENT.measure.number = 1;
     %originalTrackNumber = 1;
    
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - measure: %s \n', EXPERIMENT.measure.name{m});
        
        
        % total number of elements in the list
        N = granularity.number * aggregation.number * gap.number * weight.number * ...
            originalTrackNumber * EXPERIMENT.kuples.number;
        
        % preallocate vectors for each measure
        data.(EXPERIMENT.measure.id{m}).apc = NaN(1, N);  % the apc data
        data.(EXPERIMENT.measure.id{m}).tau = NaN(1, N);  % the tau data
        data.(EXPERIMENT.measure.id{m}).rmse = NaN(1, N);  % the apc data
        subject = cell(1, N);  % grouping variable for the subjects (kuple)
        factorA = cell(1, N);  % grouping variable for factorA (granularity)
        factorB = cell(1, N);  % grouping variable for factorB (aggregation)
        factorC = cell(1, N);  % grouping variable for factorC (gap)
        factorD = cell(1, N);  % grouping variable for factorC (weight)
        factorE = cell(1, N);  % grouping variable for factorE (track)
        
        % the current element in the list
        currentElement = 1;
        
        % for each runset
        for r = 1:originalTrackNumber
            
            originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
                        
            fprintf('    * original track of the runs: %s \n', originalTrackShortID{r});
            
            for g = 1:granularity.number
                for a = 1:aggregation.number
                    for gp = 1:gap.number
                        for w = 1:weight.number
                            
                            e = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', granularity.list{g}, aggregation.list{a}, gap.list{gp}, weight.list{w});
                            
                            fprintf('      # experiment: %s \n', e);
                            
                            
                            % for each k-uple
                            for k = 1:EXPERIMENT.kuples.number
                                
                                fprintf('        k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                
                                try
                                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID), apcID, tauID, rmseID);
                                    
                                    eval(sprintf('data.(''%1$s'').apc(currentElement) = %2$s.mean;', EXPERIMENT.measure.id{m}, apcID));
                                    eval(sprintf('data.(''%1$s'').tau(currentElement) = %2$s.mean;', EXPERIMENT.measure.id{m}, tauID));
                                    eval(sprintf('data.(''%1$s'').rmse(currentElement) = %2$s.mean;', EXPERIMENT.measure.id{m}, rmseID));
                                    eval(sprintf('subject{currentElement} = ''k%02d'';', EXPERIMENT.kuples.sizes(k)));
                                    eval(sprintf('factorA{currentElement} = ''%s'';', granularity.list{g}));
                                    eval(sprintf('factorB{currentElement} = ''%s'';', aggregation.list{a}));
                                    eval(sprintf('factorC{currentElement} = ''%s'';', gap.list{gp}));
                                    eval(sprintf('factorD{currentElement} = ''%s'';', weight.list{w}));
                                    eval(sprintf('factorE{currentElement} = ''%s'';', originalTrackShortID{r}));
                                    
                                    clear(apcID, tauID, rmseID);
                                    
                                catch exception
                                     % do nothing
                                    
                                end;

                                currentElement = currentElement + 1;
                                
                            end; % kuples
                            
                        end; % weight
                    end; % gap
                end; % aggregation
            end; % granularity
            
        end; % for each runset
    end % measures
    
    
    
    
    fprintf('+ Computing the analyses\n');    

    % the model = Kuple + Granularity + Aggregation + Gap + RunSet + 
    %             Granularity*Aggregation + Granularity*Gap + Aggregation*Gap + 
    %             Granularity*RunSet + Aggregation*RunSet + Gap*RunSet +
    %             Granularity*Aggregation*Gap
    % 
    m = [1 0 0 0 0; ...
         0 1 0 0 0; ...
         0 0 1 0 0; ...
         0 0 0 1 0; ...
         0 0 0 0 1; ...
         0 1 1 0 0; ...
         0 1 0 1 0; ...
         0 0 1 1 0; ...
         0 1 0 0 1; ...
         0 0 1 0 1; ...
         0 0 0 1 1; ...
         0 1 1 1 0];
    
    % compute a 4-way ANOVA with repeated measures and mixed effects, 
    % considering topics as random effects and stop lists, stemmers, 
    % models, and query length as fixed effects
    [~, tbl, stats] = anovan(data.ap.apc, {subject, factorA, factorB, factorC, factorD}, 'Model', m, ...
        'VarNames', {'Kuple', 'Granularity', 'Aggregation', 'Gap', 'Weight'}, ...
        'alpha', 0.05, 'display', 'off');
    

    fprintf('\n\n######## Total elapsed time for computing ANOVA analyses on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
