%% aw_us_mono_sgl_msr
% 
% Compute aware unsupervised mono-feature weights with granularity single
% and aggregation measure.

%% Synopsis
%
%   [A] = aw_us_mono_sgl_msr(DATA)
%  
%
% *Parameters*
%
% * *|DATA|* - a struct containing all the needed information.
%
%
% *Returns*
%
% * *|A|* - a TxP matrix indicating the weight of P assessors for T topics.

%

%% References
% 
% Please refer to:
%
% * Ferrante, M., Ferro, N., and Maistro, M. (2015). UNIPD Internal Report.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [A] = aw_us_mono_sgl_msr(DATA)

    fprintf('    * weighting %s gap with %s, %s, and %s measures\n', DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.EXPERIMENT.tag.(DATA.tag).und, DATA.EXPERIMENT.tag.(DATA.tag).uni, DATA.EXPERIMENT.tag.(DATA.tag).ovr);
    
    % same weight for all topics
    und = ones(1, DATA.P);
    uni = ones(1, DATA.P);
    ovr = ones(1, DATA.P);
    
     % for each assessor pool
    for ap = 1:DATA.P
        
        undID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).und, DATA.poolLabels{ap}, DATA.shortTrackID, DATA.originalTrackShortID);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.trackID, DATA.EXPERIMENT.tag.(DATA.tag).und, undID), undID);
        
                
        uniID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).uni, DATA.poolLabels{ap}, DATA.shortTrackID, DATA.originalTrackShortID);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.trackID, DATA.EXPERIMENT.tag.(DATA.tag).uni, uniID), uniID);
        
        ovrID = DATA.EXPERIMENT.pattern.identifier.pm(DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.measure, DATA.EXPERIMENT.tag.(DATA.tag).ovr, DATA.poolLabels{ap}, DATA.shortTrackID, DATA.originalTrackShortID);
        serload(DATA.EXPERIMENT.pattern.file.pre(DATA.EXPERIMENT.tag.(DATA.tag).granularity.id, DATA.EXPERIMENT.tag.(DATA.tag).aggregation.id, DATA.EXPERIMENT.tag.(DATA.tag).gap.id, DATA.trackID, DATA.EXPERIMENT.tag.(DATA.tag).ovr, ovrID), ovrID);

        eval(sprintf('und(ap) = %1$s(:);', undID));
        eval(sprintf('uni(ap) = %1$s(:);', uniID));
        eval(sprintf('ovr(ap) = %1$s(:);', ovrID));

        clear(undID, uniID, ovrID);
        
    end;
    
    % normalize the gap in the [0, 1] range: 0 no similarity with random
    % assessor; 1 equal to the random assessor.
    und = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(und, DATA.T, DATA.R);
    uni = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(uni, DATA.T, DATA.R);
    ovr = DATA.EXPERIMENT.tag.(DATA.tag).gap.normalization(ovr, DATA.T, DATA.R);
    
    % the higher the gap, the closer the assessor to a random
    % one. Weights must be reversed
    und = 1 - und;
    uni = 1 - uni;
    ovr = 1 - ovr;

    % compute the weights
    A = DATA.EXPERIMENT.tag.(DATA.tag).weight.command(und, uni, ovr, DATA.T);
      
    % save the computed weights
    eval(sprintf('%1$s = array2table(A);', DATA.weightID));        
    eval(sprintf('%1$s.Properties.RowNames = DATA.topics;', DATA.weightID));        
    eval(sprintf('%1$s.Properties.VariableNames = DATA.poolLabels;', DATA.weightID));        
        
    sersave(DATA.EXPERIMENT.pattern.file.measure(DATA.trackID, DATA.relativePath, DATA.tag, DATA.weightID), DATA.weightID(:));
    

end
