
aggregation = {'gp'};
granularity = {'tpc'};
gap = {'tau'};
rnd = {'rnd005', 'rnd010', 'rnd015', 'rnd020', 'rnd050', 'rnd080', 'rnd085', 'rnd090', 'rnd095'};


for agg = 1:length(aggregation)
    for grt = 1:length(granularity)
        for gp = 1:length(gap)
            for rp = 1:length(rnd)
                compute_rndpool_gap('TREC_21_2012_Crowd', rnd{rp}, gap{gp}, granularity{grt}, aggregation{agg})
            end;
        end;
    end;
end;