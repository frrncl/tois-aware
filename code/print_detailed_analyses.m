%% print_detailed_analyses
% 
% Print the detailed analyses and saves them tex file.
%
%% Synopsis
%
%   [] = print_detailed_analyses(trackID, color, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|color|* - boolean indicating whether to use colors or not.
% * *|ci|* - boolean indicating whether to print confidence intervals or
% not.
% * *|varargin|* - the identifiers of the experiments to print.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = print_detailed_analyses(trackID, color, ci, varargin)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that color is a non-empty logical
    validateattributes(color,{'logical'}, {'nonempty'}, '', 'color');
    
    % check that ci is a non-empty logical
    validateattributes(ci,{'logical'}, {'nonempty'}, '', 'ci');

    tags = length(varargin);
    
    for e = 1:tags
        
        % check that tag is a non-empty string
        validateattributes(varargin{e},{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
        
        if iscell(varargin{e})
            % check that tag is a cell array of strings with one element
            assert(iscellstr(varargin{e}) && numel(varargin{e}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected tag to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{e} = char(strtrim(varargin{e}));
        varargin{e} = varargin{e}(:).';
        
    end;

    % setup common parameters
    common_parameters;
        
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('detailed_analyses', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('detailed_analyses', trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Printing detailed analyses on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - experiments: \n    * %s\n\n', strjoin(varargin, '\n    * '));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    selectedKuplesIdx = [1:9 14 19 24 29];
    selectedKuplesNumber = length(selectedKuplesIdx);
    
    fprintf('+ Loading analyses\n');
    
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        apcBase.(originalTrackShortID{r}).mean = NaN(1, EXPERIMENT.measure.number);
        apcBase.(originalTrackShortID{r}).ci = NaN(1, EXPERIMENT.measure.number);
        tauBase.(originalTrackShortID{r}).mean = NaN(1, EXPERIMENT.measure.number);
        tauBase.(originalTrackShortID{r}).ci = NaN(1, EXPERIMENT.measure.number);
        rmseBase.(originalTrackShortID{r}).mean = NaN(1, EXPERIMENT.measure.number);
        rmseBase.(originalTrackShortID{r}).ci = NaN(1, EXPERIMENT.measure.number);
        
        
        fprintf('    * base measure:\n');
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('      # %s \n', EXPERIMENT.measure.name{m});
            
            measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.tag.base.id, ...
                shortTrackID,  originalTrackShortID{r});
            
            serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.base.relativePath, EXPERIMENT.tag.base.id, measureID), apcID, tauID, rmseID);
            
            eval(sprintf('apcBase.(''%1$s'').mean(m) = %2$s.mean;', originalTrackShortID{r}, apcID));
            eval(sprintf('apcBase.(''%1$s'').ci(m) = %2$s.ci;', originalTrackShortID{r}, apcID));
            eval(sprintf('tauBase.(''%1$s'').mean(m) = %2$s.mean;', originalTrackShortID{r}, tauID));
            eval(sprintf('tauBase.(''%1$s'').ci(m) = %2$s.ci;', originalTrackShortID{r}, tauID));
            eval(sprintf('rmseBase.(''%1$s'').mean(m) = %2$s.mean;', originalTrackShortID{r}, rmseID));
            eval(sprintf('rmseBase.(''%1$s'').ci(m) = %2$s.ci;', originalTrackShortID{r}, rmseID));
            
            clear(apcID, tauID, rmseID);
        end;
        
        
        
        apc.(originalTrackShortID{r}).mean = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        apc.(originalTrackShortID{r}).ci = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        tau.(originalTrackShortID{r}).mean = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        tau.(originalTrackShortID{r}).ci = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        rmse.(originalTrackShortID{r}).mean = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        rmse.(originalTrackShortID{r}).ci = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            for e = 1:tags
                
                fprintf('      # experiment: %s \n', varargin{e});
                
                for k = 1:selectedKuplesNumber
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    
                    try
                        serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(varargin{e}).relativePath, varargin{e}, measureID), apcID, tauID, rmseID);
                        
                        eval(sprintf('apc.(''%1$s'').mean(e, k, m) = %2$s.mean;', originalTrackShortID{r}, apcID));
                        eval(sprintf('apc.(''%1$s'').ci(e, k, m) = %2$s.ci;', originalTrackShortID{r}, apcID));
                        eval(sprintf('tau.(''%1$s'').mean(e, k, m) = %2$s.mean;', originalTrackShortID{r}, tauID));
                        eval(sprintf('tau.(''%1$s'').ci(e, k, m) = %2$s.ci;', originalTrackShortID{r}, tauID));
                        eval(sprintf('rmse.(''%1$s'').mean(e, k, m) = %2$s.mean;', originalTrackShortID{r}, rmseID));
                        eval(sprintf('rmse.(''%1$s'').ci(e, k, m) = %2$s.ci;', originalTrackShortID{r}, rmseID));
                        
                        clear(apcID, tauID, rmseID);
                        
                    catch exception
                        % do nothing, just skip the missing value
                    end;
                    
                end; % kuples
                
            end; % tags
            
        end % measures
        
        
    end; % for each runset
    
    
    fprintf('+ Printing the report\n');    

    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report('detailed', trackID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{colortbl} \n');
    fprintf(fid, '\\usepackage{multirow} \n');
    fprintf(fid, '\\usepackage{lscape} \n');
    fprintf(fid, '\\usepackage{pdflscape} \n');
    fprintf(fid, '\\usepackage{longtable} \n');
    fprintf(fid, '\\usepackage[x11names]{xcolor}\n\n');

    fprintf(fid, '\\begin{document}\n\n');

    fprintf(fid, '\\title{Detailed Report on %s}\n\n', EXPERIMENT.(trackID).name);

    fprintf(fid, '\\maketitle\n\n');
    
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - %s \n', EXPERIMENT.measure.name{m});
        
        %############################################ AP Correlation Table
        fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\centering \n');
        fprintf(fid, '\\footnotesize \n');
        
        fprintf(fid, '\\begin{longtable}{|l||l|*{12}{|c}|c|} \n');
        fprintf(fid, '\\caption{AP correlation for %s on collection %s using runs from %s and %s.\\label{tab:apc_%s}} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2}, ...
            EXPERIMENT.measure.id{m});
                
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\multicolumn{15}{|c|}{\\textbf{%s}} \\\\ \n', EXPERIMENT.measure.fullName{m});
        fprintf(fid, '\\hline \n');
        
        if ci
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{AP Correlation -- Runs %s: %0.4f$\\pm$%0.4f; runs %s: %0.4f$\\pm$%0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, apcBase.(originalTrackShortID{1}).mean(m), apcBase.(originalTrackShortID{1}).ci(m), ...
                originalTrackShortID{2}, apcBase.(originalTrackShortID{2}).mean(m), apcBase.(originalTrackShortID{2}).ci(m));
        else
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{AP Correlation -- Runs %s: %0.4f; runs %s: %0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, apcBase.(originalTrackShortID{1}).mean(m), originalTrackShortID{2}, apcBase.(originalTrackShortID{2}).mean(m));
        end;
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\* \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endfirsthead \n');
        
        fprintf(fid, '\\caption{AP correlation for %s on collection %s using runs from %s and %s.} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2});
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\* \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endhead \n');
        
        for e = 1:tags
            
            if color
                fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
            end;
            
            if ci
                fprintf(fid, '                               & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    originalTrackShortID{1}, reshape([apc.(originalTrackShortID{1}).mean(e, :, m); apc.(originalTrackShortID{1}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, reshape([apc.(originalTrackShortID{2}).mean(e, :, m); apc.(originalTrackShortID{2}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
            else
                
                fprintf(fid, '                                & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\ \n', ...
                    originalTrackShortID{1}, apc.(originalTrackShortID{1}).mean(e, :, m));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\ \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, apc.(originalTrackShortID{2}).mean(e, :, m));
            end;
            fprintf(fid, '\\hline \n');
        end; % for each experiment
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{longtable} \n');
        
        fprintf(fid, '\\end{landscape} \n\n\n');


        %############################################ Kendall's Tau Correlation Table
        fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\centering \n');
        fprintf(fid, '\\footnotesize \n');
        
        fprintf(fid, '\\begin{longtable}{|l||l|*{12}{|c}|c|} \n');
        fprintf(fid, '\\caption{Kendall''s tau correlation for %s on collection %s using runs from %s and %s.\\label{tab:tau_%s}} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2}, ...
            EXPERIMENT.measure.id{m});
                
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\multicolumn{15}{|c|}{\\textbf{%s}} \\\\ \n', EXPERIMENT.measure.fullName{m});
        fprintf(fid, '\\hline \n');
        
        if ci
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{Kendall''s tau Correlation -- Runs %s: %0.4f$\\pm$%0.4f; runs %s: %0.4f$\\pm$%0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, tauBase.(originalTrackShortID{1}).mean(m), tauBase.(originalTrackShortID{1}).ci(m), ...
                originalTrackShortID{2}, tauBase.(originalTrackShortID{2}).mean(m), tauBase.(originalTrackShortID{2}).ci(m));
        else
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{Kendall''s tau Correlation -- Runs %s: %0.4f; runs %s: %0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, tauBase.(originalTrackShortID{1}).mean(m), originalTrackShortID{2}, tauBase.(originalTrackShortID{2}).mean(m));
        end;
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\ \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endfirsthead \n');
        
        fprintf(fid, '\\caption{Kendall''s tau correlation for %s on collection %s using runs from %s and %s.} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2});
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\ \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endhead \n');
        
        for e = 1:tags
            
            if color
                fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
            end;
            
            if ci
                fprintf(fid, '                               & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    originalTrackShortID{1}, reshape([tau.(originalTrackShortID{1}).mean(e, :, m); tau.(originalTrackShortID{1}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, reshape([tau.(originalTrackShortID{2}).mean(e, :, m); tau.(originalTrackShortID{2}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
            else
                
                fprintf(fid, '                                & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\* \n', ...
                    originalTrackShortID{1}, tau.(originalTrackShortID{1}).mean(e, :, m));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\* \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, tau.(originalTrackShortID{2}).mean(e, :, m));
            end;
            fprintf(fid, '\\hline \n');
        end; % for each experiment
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{longtable} \n');
        
        fprintf(fid, '\\end{landscape} \n\n\n');
        
        
        %############################################ RMSE Table
        fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\centering \n');
        fprintf(fid, '\\footnotesize \n');
        
        fprintf(fid, '\\begin{longtable}{|l||l|*{12}{|c}|c|} \n');
        fprintf(fid, '\\caption{Root Mean Squared Error (RMSE) for %s on collection %s using runs from %s and %s.\\label{tab:rmse_%s}} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2}, ...
            EXPERIMENT.measure.id{m});
                
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\multicolumn{15}{|c|}{\\textbf{%s}} \\\\ \n', EXPERIMENT.measure.fullName{m});
        fprintf(fid, '\\hline \n');
        
        if ci
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{Root Mean Squared Error (RMSE) -- Runs %s: %0.4f$\\pm$%0.4f; runs %s: %0.4f$\\pm$%0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, rmseBase.(originalTrackShortID{1}).mean(m), rmseBase.(originalTrackShortID{1}).ci(m), ...
                originalTrackShortID{2}, rmseBase.(originalTrackShortID{2}).mean(m), rmseBase.(originalTrackShortID{2}).ci(m));
        else
            fprintf(fid, '\\multirow{2}{*}{\\centering\\textbf{Method}} & \\multirow{2}{*}{\\textbf{Runs}} & \\multicolumn{13}{c|}{\\textbf{Root Mean Squared Error (RMSE) -- Runs %s: %0.4f; runs %s: %0.4f}} \\\\ \n', ...
                originalTrackShortID{1}, rmseBase.(originalTrackShortID{1}).mean(m), originalTrackShortID{2}, rmseBase.(originalTrackShortID{2}).mean(m));
        end;
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\ \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endfirsthead \n');
        
        fprintf(fid, '\\caption{Root Mean Squared Error (RMSE) for %s on collection %s using runs from %s and %s.} \\\\ \n', ...
            EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).name, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{1}, ...
            EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{2});
        
        fprintf(fid, '\\cline{3-15} \n');
        fprintf(fid, ' &   & \\textbf{k=2} & \\textbf{k=3} & \\textbf{k=4} & \\textbf{k=5} & \\textbf{k=6} & \\textbf{k=7} & \\textbf{k=8} & \\textbf{k=9} & \\textbf{k=10} & \\textbf{k=15} & \\textbf{k=20} & \\textbf{k=25} & \\textbf{k=30} \\\\ \n');
        fprintf(fid, '\\hline \n');
        fprintf(fid, '\\endhead \n');
        
        for e = 1:tags
            
            if color
                fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
            end;
            
            if ci
                fprintf(fid, '                               & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    originalTrackShortID{1}, reshape([rmse.(originalTrackShortID{1}).mean(e, :, m); rmse.(originalTrackShortID{1}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f & %0.4f$\\pm$%0.4f \\\\* \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, reshape([rmse.(originalTrackShortID{2}).mean(e, :, m); rmse.(originalTrackShortID{2}).ci(e, :, m)], 1, 2*selectedKuplesNumber));
            else
                
                fprintf(fid, '                                & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\* \n', ...
                    originalTrackShortID{1}, rmse.(originalTrackShortID{1}).mean(e, :, m));
                
                if color
                    fprintf(fid, '\\rowcolor{%s} \n', EXPERIMENT.tag.(varargin{e}).latex.color);
                end;
                
                fprintf(fid, '\\multirow{-2}{*}{\\textbf{%s}} & %s & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f & %0.4f \\\\* \n', ...
                    EXPERIMENT.tag.(varargin{e}).latex.label, originalTrackShortID{2}, rmse.(originalTrackShortID{2}).mean(e, :, m));
            end;
            fprintf(fid, '\\hline \n');
        end; % for each experiment
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{longtable} \n');
        
        fprintf(fid, '\\end{landscape} \n\n\n');
        
        
    end;
            
    fprintf(fid, '\\end{document}');
    
    fclose(fid);
    

    fprintf('\n\n######## Total elapsed time for printing detailed analyses on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
