%% analyse_aware_unsupervised_measures
% 
% Analyses AWARE unsupervised measures at different k-uples sizes and saves 
% them to a |.mat| file.

%% Synopsis
%
%   [] = analyse_aware_unsupervised_measures(trackID, tag)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
% * *|startKupleSet|* - the index of the start kuples set. Optional.
% * *|endKupleSet|* - the index of the end kuples set. Optional.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * Ferrante, M., Ferro, N., and Maistro, M. (2015). UNIPD Internal Report.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = patch_names(trackID, tag, startKupleSet, endKupleSet)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
    
    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
    
    if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
        
    if nargin == 4
        validateattributes(startKupleSet, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.kuples.number }, '', 'startKupleSet');
        
        validateattributes(endKupleSet, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startKupleSet, '<=', EXPERIMENT.kuples.number }, '', 'endKupleSet');

    else 
        startKupleSet = 1;
        endKupleSet = EXPERIMENT.kuples.number;
    end;
    
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Patch names %s on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start k-uple set %d\n', startKupleSet);
    fprintf('    * end k-uple set %d\n', endKupleSet);
    
    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        
        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            startMeasure = tic;
            
            fprintf('  - patching name of %s %s\n', tag, EXPERIMENT.measure.name{m});
            
            
            % for each k-uple
            for k = startKupleSet:endKupleSet
                
                start = tic;
                
                fprintf('    * patching k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
                
                oldMeasureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.oldKuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                                
                oldTauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.oldKuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                oldApcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.oldKuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                oldRmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.oldKuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, [tag '.old'], oldMeasureID), oldTauID(:), oldApcID(:), oldRmseID(:));
                
                newMeasureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                                
                newTauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                newApcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                newRmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                                
                
                eval(sprintf('%1$s = %2$s;', newApcID, oldApcID));
                eval(sprintf('%1$s = %2$s;', newTauID, oldTauID));
                eval(sprintf('%1$s = %2$s;', newRmseID, oldRmseID));
                
                sersave(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(tag).relativePath, tag, newMeasureID), newTauID(:), newApcID(:), newRmseID(:));
                
                clear(oldMeasureID, oldTauID, oldApcID, oldRmseID, ...
                    newMeasureID, newTauID, newApcID, newRmseID);
                
                fprintf('      # elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            end; % k-uple size
                        
            fprintf('    * elapsed time for measure %s %s: %s\n', tag, EXPERIMENT.measure.name{m}, elapsedToHourMinutesSeconds(toc(startMeasure)));
            
        end; % for each measure
        
    end; % for runset
    
    
    fprintf('\n\n######## Total elapsed time for patching names %s on collection %s (%s): %s ########\n\n', ...
        tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;
    

end
