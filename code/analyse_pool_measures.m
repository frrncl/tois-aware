%% analyse_pool_measures
% 
% Analyses measures based on learned pools, e.g. majority vote at different 
% k-uples sizes and saves them to a |.mat| file.

%% Synopsis
%
%   [] = analyse_pool_measures(trackID, tag)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|tag|* - the tag of the conducted experiment.
%
%
% *Returns*
%
% Nothing
%

%% References
% 
% Please refer to:
%
% * Ferrante, M., Ferro, N., and Maistro, M. (2015). UNIPD Internal Report.
% 
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_pool_measures(trackID, tag)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that identifier is a non-empty string
    validateattributes(tag,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'tag');
    
    if iscell(tag)
        % check that identifier is a cell array of strings with one element
        assert(iscellstr(tag) && numel(tag) == 1, ...
            'MATTERS:IllegalArgument', 'Expected Identifier to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    tag = char(strtrim(tag));
    tag = tag(:).';
    
    % setup common parameters
    common_parameters;
    
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.analyseMeasures(tag, trackID));
    diary(EXPERIMENT.pattern.logFile.analyseMeasures(tag, trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Analysing %s measures on collection %s (%s) ########\n\n', tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
    fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % for each runset
    for r = 1:originalTrackNumber        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
    end;
    
    % load the kuples
    serload(EXPERIMENT.pattern.file.kuples(trackID, EXPERIMENT.(trackID).shortID));
    
    % load data
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'T');
    
    % for each runset
    for r = 1:EXPERIMENT.(trackID).collection.runSet.originalTrackID.number
        
        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.(trackID).collection.runSet.originalTrackID.id{r});
        
        
        % for each measure
        for m = 1:EXPERIMENT.measure.number
            
            startMeasure = tic;
            
            fprintf('  - analysing %s %s\n', EXPERIMENT.tag.base.id, EXPERIMENT.measure.name{m});
            
            % loading the gold standard measure            
            goldMeasureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, EXPERIMENT.tag.base.id, EXPERIMENT.label.goldPool, ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                      
            fprintf('    * loading gold standard measure: %s\n', goldMeasureID);
            
            serload(EXPERIMENT.pattern.file.measure(trackID, EXPERIMENT.tag.base.id, goldMeasureID), goldMeasureID);
            
            % determine the total number of runs
            evalf(@width, {goldMeasureID}, {'R'}) 
            
            % for each k-uple
            for k = 1:EXPERIMENT.kuples.number
                
                start = tic;
                
                fprintf('    * k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
                
                % determine the size of the current kuple
                evalf(EXPERIMENT.command.computePoolMeasures.kupleSize, ...
                    {kuplesIdentifiers{k}}, ...
                    {'KK'});
                
                % the raw data, each TxR plane is a measure for T topics and R runs
                % to be weighted by assessors' scores and the M planes correspond
                % to the different assessors
                data = NaN(T, R, KK);
                
                % for each k-uple in the set copy the corresponding measure
                for kk = 1:KK
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, ...
                        EXPERIMENT.pattern.identifier.sampledKuple(EXPERIMENT.kuples.sizes(k), kk), shortTrackID, originalTrackShortID{r});
                    
                    
                    serload(EXPERIMENT.pattern.file.measure(trackID, tag, measureID), measureID);
                    
                    eval(sprintf('data(:, :, kk) = %1$s{:, :};', measureID));
                    
                    clear(measureID);
                end;
                
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, tag, EXPERIMENT.pattern.identifier.kuple(k), ...
                    EXPERIMENT.(trackID).shortID,  EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r});
                
                
                evalf(EXPERIMENT.command.analyse, ...
                    {goldMeasureID, 'data'}, ...
                    {rmseID, tauID, apcID});
                                
                sersave(EXPERIMENT.pattern.file.analysis(trackID, tag, measureID), tauID(:), apcID(:), rmseID(:));
                
                clear(measureID, tauID, apcID, rmseID);
                
                fprintf('      # elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            end; % k-uple size
            
            clear(goldMeasureID);
            
            fprintf('  - elapsed time for measure %s %s: %s\n', tag, EXPERIMENT.measure.name{m}, elapsedToHourMinutesSeconds(toc(startMeasure)));
            
        end; % for each measure
        
    end; % for each runset
    

    fprintf('\n\n######## Total elapsed time for analysing %s measures on collection %s (%s): %s ########\n\n', ...
        tag, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    diary off;


end
