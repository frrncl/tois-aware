%% list_Systems
% 
% Lists the different approaches.
%
%% Synopsis
%
%   list = list_Systems()
%  
%

%
% *Returns*
%
% * *|list|* - the list of the different approaches.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function list = list_Systems()

    % setup common parameters
    common_parameters;

    % total number of elements in the list
    N = EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number;

    % the labels of the experiments
    list = cell(1, N);

    % the current element in the list
    currentElement = 1;

    for g = 1:EXPERIMENT.taxonomy.granularity.number
        for a = 1:EXPERIMENT.taxonomy.aggregation.number
            for gp = 1:EXPERIMENT.taxonomy.gap.number
                for w = 1:EXPERIMENT.taxonomy.weight.number

                    list{currentElement} = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', ...
                        EXPERIMENT.taxonomy.granularity.list{g}, ...
                        EXPERIMENT.taxonomy.aggregation.list{a}, ...
                        EXPERIMENT.taxonomy.gap.list{gp}, ...
                        EXPERIMENT.taxonomy.weight.list{w});


                    currentElement = currentElement + 1;

                end; % weight
            end; % gap
        end; % aggregation
    end; % granularity

end