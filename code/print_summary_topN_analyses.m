%% print_summary_topN_analyses
% 
% Print the summary analyses reporting the top N weighting schemes and 
% saves them to a tex file.
%
%% Synopsis
%
%   [] = print_summary_topN_analyses(trackID, color, topN, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|color|* - boolean indicating whether use colors or not.
% * *|topN|* - the number of top N experiments to report.
% * *|varargin|* - the identifiers of the experiments to print.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = print_summary_topN_analyses(trackID, color, topN, varargin)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that color is a non-empty logical
    validateattributes(color,{'logical'}, {'nonempty'}, '', 'color');

    experiments = length(varargin);
    
    % check that top is a non-empty logical
    validateattributes(topN, {'numeric'}, {'nonempty', 'integer', 'scalar', '>', 0, '<=', experiments}, '', 'top');
    
    for e = 1:experiments
        
        % check that experiment is a non-empty string
        validateattributes(varargin{e},{'char', 'cell'}, {'nonempty', 'vector'}, '', 'experiment');
        
        if iscell(varargin{e})
            % check that experiment is a cell array of strings with one element
            assert(iscellstr(varargin{e}) && numel(varargin{e}) == 1, ...
                'MATTERS:IllegalArgument', 'Expected experiment to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        varargin{e} = char(strtrim(varargin{e}));
        varargin{e} = varargin{e}(:).';
        
    end;

    % setup common parameters
    common_parameters;
        
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport(sprintf('top-%d-best-approaches', topN), trackID));
    diary(EXPERIMENT.pattern.logFile.printReport(sprintf('top-%d-best-approaches', topN), trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Printing top %d best approaches on collection %s (%s) ########\n\n', topN, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
       

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - top %d weighting schemes\n', topN);
    fprintf('  - experiments: \n    * %s\n\n', strjoin(varargin, '\n    * '));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    selectedKuplesIdx = [1:9 14 19 24 29];
    selectedKuplesNumber = length(selectedKuplesIdx);

    tags = length(varargin);
        
    fprintf('+ Loading analyses\n');
        
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
                
        apc.(originalTrackShortID{r}) = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        tau.(originalTrackShortID{r}) = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        rmse.(originalTrackShortID{r}) = NaN(tags, selectedKuplesNumber, EXPERIMENT.measure.number);
        
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            for e = 1:tags
                
                fprintf('      # experiment: %s \n', varargin{e});
                
                for k = 1:selectedKuplesNumber
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, varargin{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    
                    try
                        serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(varargin{e}).relativePath, varargin{e}, measureID), apcID, tauID, rmseID);
                        
                        eval(sprintf('apc.(''%1$s'')(e, k, m) = %2$s.mean;', originalTrackShortID{r}, apcID));
                        eval(sprintf('tau.(''%1$s'')(e, k, m) = %2$s.mean;', originalTrackShortID{r}, tauID));
                        eval(sprintf('rmse.(''%1$s'')(e, k, m) = %2$s.mean;', originalTrackShortID{r}, rmseID));
                        
                        clear(apcID, tauID, rmseID);
                        
                    catch exception
                        % do nothing, just skip the missing value
                    end;
                    
                end; % kuples
                
            end; % tags
            
        end % measures
        
        % Remove NaNs due to missing data which cause issues when ordering
        apc.(originalTrackShortID{r})(isnan(apc.(originalTrackShortID{r}))) = -Inf;
        tau.(originalTrackShortID{r})(isnan(tau.(originalTrackShortID{r}))) = -Inf;
        rmse.(originalTrackShortID{r})(isnan(rmse.(originalTrackShortID{r}))) = -Inf;
        
    end; % for each runset
    
    fprintf('+ Sorting analyses\n');
    
    % for each runset
    for r = 1:originalTrackNumber
        
        [~,apcIDX.(originalTrackShortID{r})] = sort(apc.(originalTrackShortID{r}), 'descend');
        [~, tauIDX.(originalTrackShortID{r})] = sort(tau.(originalTrackShortID{r}), 'descend');
        [~, rmseIDX.(originalTrackShortID{r})] = sort(rmse.(originalTrackShortID{r}), 'ascend');
        
    end;
    
    fprintf('+ Printing the report\n');    

    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report('summary', trackID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{multirow} \n');
    fprintf(fid, '\\usepackage{colortbl} \n');
    fprintf(fid, '\\usepackage{lscape} \n');
    fprintf(fid, '\\usepackage{pdflscape} \n');        
    fprintf(fid, '\\usepackage[x11names]{xcolor}\n\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    

    fprintf(fid, '\\begin{document}\n\n');

    fprintf(fid, '\\title{Summary Report on Top %d Estimators for Collection %s}\n\n', topN, EXPERIMENT.(trackID).name);

    fprintf(fid, '\\maketitle\n\n');

    %############################################ AP Correlation Table
%    fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    fprintf(fid, '\\vspace*{-12em} \n');
    fprintf(fid, '\\hspace*{-16.5em} \n');

    fprintf(fid, '\\begin{tabular}{|l|*{%d}{|c|}} \n', EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\multicolumn{%d}{|c|}{\\textbf{AP Correlation}} \\\\ \n', 1 + EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');  
    
    for m = 1:EXPERIMENT.measure.number
        fprintf(fid, '    & \\multicolumn{2}{|c|}{\\textbf{%s}} ', EXPERIMENT.measure.name{m});        
    end;
    
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    
    
    for m = 1:EXPERIMENT.measure.number
        % for each runset
        for r = 1:originalTrackNumber            
            fprintf(fid, '    & \\textbf{%s} ', originalTrackShortID{r});            
        end;
        
        
    end;
        
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\hline \n');

    for k = 1:selectedKuplesNumber

        fprintf(fid, ' \\multirow{%d}*{\\textbf{k = %d}} ', topN, EXPERIMENT.kuples.sizes(selectedKuplesIdx(k)));
        
        for top = 1:topN
            
            for m = 1:EXPERIMENT.measure.number
                
                % for each runset
                for r = 1:originalTrackNumber
                    
                    if color
                        fprintf(fid, '& \\cellcolor{%s} %s ', EXPERIMENT.tag.(varargin{apcIDX.(originalTrackShortID{r})(top, k, m)}).latex.color, EXPERIMENT.tag.(varargin{apcIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    else
                        fprintf(fid, '& %s ', EXPERIMENT.tag.(varargin{apcIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    end;
                    
                end % for each runset
                
            end; % for each measure
            
            fprintf(fid, '\\\\ \n');
        end; % for each top value
        
        fprintf(fid, '\\hline \n');
        
    end;

    fprintf(fid, '\\hline \n');

    fprintf(fid, '\\end{tabular} \n');   
        
    fprintf(fid, '\\caption{Top %d estimators on collection %s according to AP Correlation.} \n', ...
        topN, EXPERIMENT.(trackID).name);   

    fprintf(fid, '\\end{table} \n\n\n');   
    
%    fprintf(fid, '\\end{landscape}  \n');
    
 
    %############################################ Kendall's tau Correlation Table
%    fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    fprintf(fid, '\\vspace*{-12em} \n');
    fprintf(fid, '\\hspace*{-16.5em} \n');

    fprintf(fid, '\\begin{tabular}{|l|*{%d}{|c|}} \n', EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\multicolumn{%d}{|c|}{\\textbf{Kendall''s tau Correlation}} \\\\ \n', 1 + EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');  
    
    for m = 1:EXPERIMENT.measure.number
        fprintf(fid, '    & \\multicolumn{2}{|c|}{\\textbf{%s}} ', EXPERIMENT.measure.name{m});        
    end;
    
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    
    
    for m = 1:EXPERIMENT.measure.number
        % for each runset
        for r = 1:originalTrackNumber            
            fprintf(fid, '    & \\textbf{%s} ', originalTrackShortID{r});            
        end;
        
        
    end;
        
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\hline \n');

    for k = 1:selectedKuplesNumber

        fprintf(fid, ' \\multirow{%d}*{\\textbf{k = %d}} ', topN, EXPERIMENT.kuples.sizes(selectedKuplesIdx(k)));
        
        for top = 1:topN
            
            for m = 1:EXPERIMENT.measure.number
                
                % for each runset
                for r = 1:originalTrackNumber
                    
                    if color
                        fprintf(fid, '& \\cellcolor{%s} %s ', EXPERIMENT.tag.(varargin{tauIDX.(originalTrackShortID{r})(top, k, m)}).latex.color, EXPERIMENT.tag.(varargin{tauIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    else
                        fprintf(fid, '& %s ', EXPERIMENT.tag.(varargin{tauIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    end;
                    
                end % for each runset
                
            end; % for each measure
            
            fprintf(fid, '\\\\ \n');
        end; % for each top value
        
        fprintf(fid, '\\hline \n');
        
    end;

    fprintf(fid, '\\hline \n');

    fprintf(fid, '\\end{tabular} \n');   
        
    fprintf(fid, '\\caption{Top %d estimators on collection %s according to Kendall''s tau Correlation.} \n', ...
        topN, EXPERIMENT.(trackID).name);   

    fprintf(fid, '\\end{table} \n\n\n');   
    
%    fprintf(fid, '\\end{landscape}  \n');


    %############################################ RMSE Table
%    fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    fprintf(fid, '\\vspace*{-12em} \n');
    fprintf(fid, '\\hspace*{-16.5em} \n');

    fprintf(fid, '\\begin{tabular}{|l|*{%d}{|c|}} \n', EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\multicolumn{%d}{|c|}{\\textbf{Root Mean Squared Error (RMSE)}} \\\\ \n', 1 + EXPERIMENT.measure.number*originalTrackNumber);
    fprintf(fid, '\\hline \n');  
    
    for m = 1:EXPERIMENT.measure.number
        fprintf(fid, '    & \\multicolumn{2}{|c|}{\\textbf{%s}} ', EXPERIMENT.measure.name{m});        
    end;
    
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    
    
    for m = 1:EXPERIMENT.measure.number
        % for each runset
        for r = 1:originalTrackNumber            
            fprintf(fid, '    & \\textbf{%s} ', originalTrackShortID{r});            
        end;
        
        
    end;
        
    fprintf(fid, ' \\\\ \n');
    fprintf(fid, '\\hline \n');
    fprintf(fid, '\\hline \n');

    for k = 1:selectedKuplesNumber

        fprintf(fid, ' \\multirow{%d}*{\\textbf{k = %d}} ', topN, EXPERIMENT.kuples.sizes(selectedKuplesIdx(k)));
        
        for top = 1:topN
            
            for m = 1:EXPERIMENT.measure.number
                
                % for each runset
                for r = 1:originalTrackNumber
                    
                    if color
                        fprintf(fid, '& \\cellcolor{%s} %s ', EXPERIMENT.tag.(varargin{rmseIDX.(originalTrackShortID{r})(top, k, m)}).latex.color, EXPERIMENT.tag.(varargin{rmseIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    else
                        fprintf(fid, '& %s ', EXPERIMENT.tag.(varargin{rmseIDX.(originalTrackShortID{r})(top, k, m)}).latex.label);
                    end;
                    
                end % for each runset
                
            end; % for each measure
            
            fprintf(fid, '\\\\ \n');
        end; % for each top value
        
        fprintf(fid, '\\hline \n');
        
    end;

    fprintf(fid, '\\hline \n');

    fprintf(fid, '\\end{tabular} \n');   
        
    fprintf(fid, '\\caption{Top %d estimators on collection %s according to Root Mean Squared Error (RMSE).} \n', ...
        topN, EXPERIMENT.(trackID).name);   

    fprintf(fid, '\\end{table} \n\n\n');   
    
%    fprintf(fid, '\\end{landscape}  \n');




    fprintf(fid, '\\end{document}');

    fclose(fid);

     fprintf('\n\n######## Total elapsed time for printing top %d best approaches on collection %s (%s): %s ########\n\n', ...
            topN, EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));


    diary off;

end
