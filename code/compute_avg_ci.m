%% compute_avg_ci
% 
% Computes the average confidence intervals.
%
%% Synopsis
%
%   [] = compute_avg_ci(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = compute_avg_ci(trackID)

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % setup common parameters
    common_parameters;
                
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('computeAvgCI', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('computeAvgCI', trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Computing average CI on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
    
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    fprintf('+ Loading analyses\n');
        
     %EXPERIMENT.measure.number = 1;
     %originalTrackNumber = 1;
    
    for m = 1:EXPERIMENT.measure.number
        
        fprintf('  - measure: %s \n', EXPERIMENT.measure.name{m});
        
        
        % total number of elements in the list
        N = granularity.number * aggregation.number * gap.number * weight.number * ...
            originalTrackNumber * EXPERIMENT.kuples.number * EXPERIMENT.measure.number;
        
        % preallocate vectors for each measure
        data.apc.mean = NaN(1, N);  % the apc data
        data.apc.ci = NaN(1, N);    % the apc data
        data.rmse.mean = NaN(1, N); % the rmse data
        data.rmse.ci = NaN(1, N);   % the rmse data
        
        % the current element in the list
        currentElement = 1;
        
        % for each runset
        for r = 1:originalTrackNumber
            
            originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
                        
            fprintf('    * original track of the runs: %s \n', originalTrackShortID{r});
            
            for g = 1:granularity.number
                for a = 1:aggregation.number
                    for gp = 1:gap.number
                        for w = 1:weight.number
                            
                            e = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', granularity.list{g}, aggregation.list{a}, gap.list{gp}, weight.list{w});
                            
                            fprintf('      # experiment: %s \n', e);
                            
                            
                            % for each k-uple
                            for k = 1:EXPERIMENT.kuples.number
                                
                                %fprintf('        k-uples: k%02d \n', EXPERIMENT.kuples.sizes(k));
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(k), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                
                                try
                                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID), ...
                                            {apcID, 'apc';
                                             rmseID, 'rmse'});
                                         
                                    data.apc.mean(currentElement) = apc.mean;
                                    data.apc.ci(currentElement) = apc.ci;
                                    
                                    data.rmse.mean(currentElement) = rmse.mean;
                                    data.rmse.ci(currentElement) = rmse.ci;
                                    
                                catch exception
                                     % do nothing
                                     fprintf('        File not found: %s\n', EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID));
                                    
                                end;

                                clear apc rmse;
                                
                                currentElement = currentElement + 1;
                                
                            end; % kuples
                            
                        end; % weight
                    end; % gap
                end; % aggregation
            end; % granularity
            
        end; % for each runset
    end % measures
    
    fprintf('\n+ Average confidence intervals\n');
    
    fprintf('  - apc: %f \n', nanmean(data.apc.ci ./ data.apc.mean));
    fprintf('  - rmse: %f \n', nanmean(data.rmse.ci ./ data.rmse.mean));
 

    fprintf('\n\n######## Total elapsed time for computing average CI on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end
