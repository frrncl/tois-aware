%% analyse_multiple_comparisons
% 
% Computes the Tukey HSD comparison plots and saves them to a pdf file.
%
%% Synopsis
%
%   [] = analyse_multiple_comparisons(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


function [] = analyse_multiple_comparisons(trackID)

    % setup common parameters
    common_parameters;
    
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
           
    % turn on logging
    delete(EXPERIMENT.pattern.logFile.printReport('analyse_multiple_comparisons', trackID));
    diary(EXPERIMENT.pattern.logFile.printReport('analyse_multiple_comparisons', trackID));
    
    % start of overall computations
    startComputation = tic;
    
    fprintf('\n\n######## Analyse multiple comparisons on collection %s (%s) ########\n\n', EXPERIMENT.(trackID).name, EXPERIMENT.label.paper);
       

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    
    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;    
    selectedKuplesIdx = [2 3 4 5 10 20 30] - 1;
    selectedKuplesIdx = 1:29;
    selectedKuplesNumber = length(selectedKuplesIdx);
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    
    % Load the total number of pools
    serload(EXPERIMENT.pattern.file.dataset(trackID, EXPERIMENT.(trackID).shortID), 'P');
        
    
    % total number of elements in the list 
    N = EXPERIMENT.taxonomy.granularity.number * EXPERIMENT.taxonomy.aggregation.number * ...
        EXPERIMENT.taxonomy.gap.number * EXPERIMENT.taxonomy.weight.number + ...
        EXPERIMENT.taxonomy.baseline.number;
    
    % the labels of the experiments
    labels = cell(1, N);
    
    
    fprintf('+ Loading analyses\n');
    
    apc.id = 'apc';
    rmse.id = 'rmse';
    
    
% originalTrackNumber = 1;
%   EXPERIMENT.measure.number = 1;    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};
        
        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});
        
        for m = 1:EXPERIMENT.measure.number
            
            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});
            
            for k = 1:selectedKuplesNumber
                
                fprintf('      # k-uple: %s \n', EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)));
                
                % total number of possible kuples of the selected size
                K = nchoosek(P, EXPERIMENT.kuples.sizes(selectedKuplesIdx(k)));
                
                % number of kuples to be actually sampled
                samples = min([EXPERIMENT.kuples.samples K]);
                
                apc.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k))) ...
                    = NaN(samples, N);
                rmse.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k))) ...
                    = NaN(samples, N);
                
                % the current element in the list
                currentElement = 1;
                
                for g = 1:EXPERIMENT.taxonomy.granularity.number
                    for a = 1:EXPERIMENT.taxonomy.aggregation.number
                        for gp = 1:EXPERIMENT.taxonomy.gap.number
                            for w = 1:EXPERIMENT.taxonomy.weight.number
                                
                                e = EXPERIMENT.taxonomy.tag('aw', 'us', 'mono', ...
                                    EXPERIMENT.taxonomy.granularity.list{g}, ...
                                    EXPERIMENT.taxonomy.aggregation.list{a}, ...
                                    EXPERIMENT.taxonomy.gap.list{gp}, ...
                                    EXPERIMENT.taxonomy.weight.list{w});
                                labels{currentElement} = EXPERIMENT.tag.(e).matlab.label;
                                %fprintf('        experiment: %s \n', e);
                                
                                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},e, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m}, e, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                                    shortTrackID,  originalTrackShortID{r});
                                
                                try
                                    serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID), ...
                                        {apcID, 'apcTmp';
                                        rmseID, 'rmseTmp'});
                                    
                                    apc.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = apcTmp.data(:);
                                    rmse.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = rmseTmp.data(:);
                                    
                                catch exception
                                    fprintf('        File not found: %s\n', EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(e).relativePath, e, measureID));
                                    
                                    % remove not found tags
                                    
                                    labels(currentElement) = [];
                                    
                                    apc.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = [];
                                    rmse.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = [];
                                    
                                end;
                                
                                currentElement = currentElement + 1;
                                
                                clear apcTmp rmseTmp;
                                
                            end; % weight
                        end; % gap
                    end; % aggregation
                end; % granularity
                
                % add the baselines
                for e = 1:EXPERIMENT.taxonomy.baseline.number
                    
                    labels{currentElement} = EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).matlab.label;
                    
                    
                    measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.measure.id{m},  EXPERIMENT.taxonomy.baseline.list{e}, EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)), ...
                        shortTrackID,  originalTrackShortID{r});
                    
                    try
                        serload(EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.(EXPERIMENT.taxonomy.baseline.list{e}).relativePath,  EXPERIMENT.taxonomy.baseline.list{e}, measureID), ...
                            {apcID, 'apcTmp';
                            rmseID, 'rmseTmp'});
                        
                        apc.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = apcTmp.data(:);
                        rmse.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = rmseTmp.data(:);
                        
                    catch exception
                        fprintf('        File not found: %s\n', EXPERIMENT.pattern.file.analysis(trackID, EXPERIMENT.tag.( EXPERIMENT.taxonomy.baseline.list{e}).relativePath,  EXPERIMENT.taxonomy.baseline.list{e}, measureID));
                        
                        % remove not found tags
                        
                        labels(currentElement) = [];
                        
                        apc.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = [];
                        rmse.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)))(:, currentElement) = [];
                        
                    end;
                    
                    currentElement = currentElement + 1;
                    
                    clear apcTmp rmseTmp
                    
                end % additional tags
                
            end; % kuples
            
        end % measures
        
    end; % for each runset
    
                         
    fprintf('+ Creating the visualization\n');   
    
    createVisualization(apc, trackID, EXPERIMENT, selectedKuplesIdx, selectedKuplesNumber, labels);
    
    createVisualization(rmse, trackID, EXPERIMENT, selectedKuplesIdx, selectedKuplesNumber, labels);
    
    fprintf('\n\n######## Total elapsed time for analysing multiple comparisons on collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
    
    
    diary off;
    
end


function [] = createVisualization(dataset, trackID, EXPERIMENT, selectedKuplesIdx, selectedKuplesNumber, labels)

    % local version of the general configuration parameters
    shortTrackID = EXPERIMENT.(trackID).shortID;
    originalTrackNumber = EXPERIMENT.(trackID).collection.runSet.originalTrackID.number;
    originalTrackShortID = cell(1, EXPERIMENT.(trackID).collection.runSet.originalTrackID.number);
    tags = length(labels);


    % keeps the handles of the figures generated by multcompare, which
    % will be then put into subplots in the final figure
    mcFigureHandles = cell(EXPERIMENT.measure.number, selectedKuplesNumber, originalTrackNumber);
    mcAxesHandles = cell(EXPERIMENT.measure.number, selectedKuplesNumber, originalTrackNumber);
    
    % upper and lower limits for the axes
    mcLimits.lower = NaN(EXPERIMENT.measure.number, selectedKuplesNumber, originalTrackNumber);
    mcLimits.upper = NaN(EXPERIMENT.measure.number, selectedKuplesNumber, originalTrackNumber);

% originalTrackNumber = 1;
%   EXPERIMENT.measure.number = 1;    
    
    
    % for each runset
    for r = 1:originalTrackNumber
        
        originalTrackShortID{r} = EXPERIMENT.(trackID).collection.runSet.originalTrackID.shortID{r};

        fprintf('  - original track of the runs: %s \n', originalTrackShortID{r});

        for m = 1:EXPERIMENT.measure.number

            fprintf('    * measure: %s \n', EXPERIMENT.measure.name{m});

            % for each kuple
            for k = 1:selectedKuplesNumber

                % the current data frame
                data = dataset.(originalTrackShortID{r}).(EXPERIMENT.measure.id{m}).(EXPERIMENT.pattern.identifier.kuple(selectedKuplesIdx(k)));
                
                switch dataset.id
                    case 'apc'                        
                        % sort the data by their descending mean
                        [~, idx] = sort(mean(data), 'descend');

                    case 'rmse'

                        % sort the data by their ascending mean
                        [~, idx] = sort(mean(data), 'ascend');

                    otherwise
                        warning('Unexpected dataset type. No plot created.')
                end

                % swaps experiments names and data to match their ordering
                data = data(: , idx);
                dataLabels = labels(idx);

                % compute a one-way ANOVA between experiments (columns)
                [~, ~, st] = anova1(data, dataLabels, 'off');

                mcFigureHandles{m, k, r} = figure('Visible', 'off');
                
                % compare all the possible pairs
                [~, ~, ~, ~] = multcompare(st,'ctype', 'hsd', 'estimate', 'column', 'alpha', 0.05, 'display','on');

                % get the current axes and set the properties
                ax = gca;
                
                mcAxesHandles{m, k, r} = ax;
                
                ax.Title.String = sprintf('k = %d', EXPERIMENT.kuples.sizes(selectedKuplesIdx(k)));
                ax.Title.FontSize = 24;
                
                ax.TickLabelInterpreter = 'none';

                ax.XLabel.String = '';

                ax.YLabel.String = '';
                ax.YTickLabel = dataLabels(end:-1:1);
                
                mcLimits.lower(m, k, r) = ax.XLim(1);
                mcLimits.upper(m, k, r) = ax.XLim(2);

                highlightTopGroup(tags, mcFigureHandles{m, k, r});

            end; % kuples

        end % measures

    end; % for each runset

    xMin = min(mcLimits.lower, [], 2);
    xMax = max(mcLimits.upper, [], 2);
    
    % copy all the separate figures to the same plot into subplots
    % for each runset
    for r = 1:originalTrackNumber

        for m = 1:EXPERIMENT.measure.number

            currentFigure = figure('Visible', 'off');
            
            xLim = [xMin(m, 1, r) xMax(m, 1, r)];

            %    axf = gca;

            %    axf.Title.String = sprintf('%s on %s', EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{r});
            %    axf.Title.FontSize = 14;


            for k = 1:selectedKuplesNumber

                % copy the plot generated by multcompare into current
                % figure
                ax = copyobj(mcAxesHandles{m, k, r}, currentFigure);
                ax.XLim = xLim;
                ax.FontSize = 18;
                
                % move it to the appropriate subplot
                subplot(1, selectedKuplesNumber, k, ax);
                 
                % close the plot generated by multcompare
                close(mcFigureHandles{m, k, r});

            end; % kuples


            ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1], ...
                'Box', 'off', 'Visible', 'off', 'Units', 'normalized', 'Clipping' , 'off');
            
            text(0.5, 1, sprintf('%s on runs of %s', EXPERIMENT.measure.fullName{m}, EXPERIMENT.(trackID).collection.runSet.originalTrackID.name{r}), ...
                'HorizontalAlignment' ,'center','VerticalAlignment', 'top', 'FontSize', 36, 'FontWeight', 'bold')
            
            switch dataset.id
                    case 'apc'
                        
                        text(0.5, 0.025,'AP Correlation', ...
                            'HorizontalAlignment' ,'center','VerticalAlignment', 'bottom', 'FontSize', 36, 'FontWeight', 'bold')

                    case 'rmse'

                        text(0.5, 0,'Root Mean Square Error (RMSE)', ...
                            'HorizontalAlignment' ,'center','VerticalAlignment', 'bottom', 'FontSize', 36, 'FontWeight', 'bold')

                    otherwise
                        warning('Unexpected dataset type. No plot created.')
            end
            
            
            text(0.05, 0.5,'Experiment', ...
                'Rotation', 90, 'HorizontalAlignment' ,'center','VerticalAlignment', 'top', 'FontSize', 36, 'FontWeight', 'bold')
            
            currentFigure.PaperPositionMode = 'auto';
            %currentFigure.PaperType = 'a0';
            %currentFigure.PaperOrientation = 'landscape';
            currentFigure.PaperUnits = 'centimeters';
            currentFigure.PaperSize = [402 32];
            currentFigure.PaperPosition = [1 1 400 30];
            %currentFigure.PaperSize = [120 32];
            %currentFigure.PaperPosition = [1 1 118 30];
            %currentFigure.PaperPosition = [0.05 0.05 0.9 0.9];

            figureID = EXPERIMENT.pattern.identifier.multcompare.figure(dataset.id, EXPERIMENT.measure.id{m}, shortTrackID, originalTrackShortID{r});

            print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, 'multcompare', figureID));

            close(currentFigure)

        end % measures

    end; % for each runset


end



function [] =  highlightTopGroup(tags, h)

    % the index of the top element in the top group
    topElement = 1;
    
    % Get bounds for the top group
    topGroupHandle = findobj(h, 'UserData', -topElement);
    if (isempty(topGroupHandle))
        return; 
    end % unexpected

    topGroupX = get(topGroupHandle, 'XData');
    topGroupLower = min(topGroupX);
    topGroupUpper = max(topGroupX);

    % arrange line styles and markers
    for e=1:tags

        % look for the line and marker of the current element
        lineHandle = findobj(h, 'UserData', -e, 'Type','Line', '-and', 'LineStyle', '-', '-and', 'Marker', 'none');
        markerHandle = findobj(h, 'UserData', e, 'Type','Line', '-and', 'LineStyle', 'none', '-and', 'Marker', 'o');

        if (isempty(lineHandle))
            continue;
        end  % unexpected

        currentElementX = get(lineHandle, 'XData');
        currentElementLower = min(currentElementX);
        currentElementUpper = max(currentElementX);
        
        % To be in the top group the upper bound of the current element
        % (CEL) must be above the lower bound of the top group (TGL) and
        % the lower bound of the current element (CEL) must be below the
        % upper bound of the top groud (TGL)
        %
        %  CEL    TGL   CEU  TGU
        %   |      |_____|____|
        %   |____________|
        %
        %  TGL   CEL  TGU     CEU
        %   |_____|____|       |
        %         |____________|
        %
        if ( currentElementUpper > topGroupLower && currentElementLower < topGroupUpper)
            lineHandle.Color = 'b';
            lineHandle.LineWidth = 1.0;
            lineHandle.Visible = 'off';
            markerHandle.MarkerFaceColor = 'b';
            markerHandle.MarkerEdgeColor = 'b';
            markerHandle.MarkerSize = 1.5;
        else
            lineHandle.Color = 'k';
            lineHandle.LineWidth = 0.5;
            lineHandle.Visible = 'off';
            markerHandle.MarkerFaceColor = 'k';
            markerHandle.MarkerEdgeColor = 'k';
            markerHandle.MarkerSize = 1;
        end
    end; % for tags

 
end


