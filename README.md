# TOIS AWARE Matlab Code #

This is the Matlab code for running the experiments described in the paper

M. Ferrante, N. Ferro, M. Maistro (2017). AWARE: Exploiting Evaluation Measures to Combine Multiple Assessors. ACM Transaction on Information Systems (TOIS).

This code is based on the MATTERS Matlab library available at: http://matters.dei.unipd.it/